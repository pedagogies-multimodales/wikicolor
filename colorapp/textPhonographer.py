# -*- encoding:utf8 -*- 
import re, sys, os, datetime, json, tempfile, csv, locale, pinyin, pymongo
from bson import json_util
from colorapp.models import Entree, DicEntry, LogStat
from collections import OrderedDict

locale.setlocale(locale.LC_ALL, "")

from sys import path as pylib #im naming it as pylib so that we won't get confused between os.path and sys.path 

pylib += [os.path.relpath(r'../phon2graph')]
from phon2graph_french import decoupage
from phon2graph_english import decoupageEn # ENGLISH
from phon2graph_mandarin import pinyin2phon, pinyin2phon2 # MANDARIN CHINESE
from .liaisons import *

# FICHIERS
phonColFile = "../phon2graph/data/api2class.json"

phonGraphFile = "../phon2graph/data/fidel_wikicolor.scsv" # "../phon2graph/data/phoneme-graphies_fr.scsv"
phonGraphFileEn = "../phon2graph/data/fidel_wikicolor_en_global.scsv" # ENGLISH
pinyin2apiFile = "../phon2graph/data/pinyin2api.json" # MANDARIN
api2classFile = "../phon2graph/data/api2class.json" # MANDARIN
pinyin2zhuyinFile = "../phon2graph/data/pinyin2zhuyin.json" # MANDARIN


########################################
########## CONNEXION MONGODB ###########
########################################
def connexion():
    mdp = ""
    with open('../private/dbmdp','r') as infile:
        mdp = infile.read()
    print('Connexion à AlemDic...')
    mongodb_client = pymongo.MongoClient("mongodb+srv://alemadmin:"+mdp+"@cluster0.6nopd.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
    print(mongodb_client.list_database_names())
    return mongodb_client

alemDb = connexion()
alemdic = alemDb['alemdic']
users_appstats = alemDb['alem-app_db']['users_appstats']
dicoFr = alemdic['dicoFr']
dicoEn = alemdic['dicoEn']
dicoZh = alemdic['dicoZh']
dicoFrLogs = alemdic['dicoFrLogs']
dicoEnLogs = alemdic['dicoEnLogs']
dicoZhLogs = alemdic['dicoZhLogs']


##### STATS DICTIONNAIRES
def getLenDic(lang):
    # if lang == "fr":
    #     alemDic = dicoFr
    # elif lang == "en":
    #     alemDic = dicoEn
    # elif lang == "zh":
    #     alemDic = dicoZh

    # query = alemDic.find()
    # lenDic = 0
    # for q in query:
    #     lenDic+=1
    # return lenDic

    # VARIANTE ÉCONOMIQUE (calculée à partir des logs)
    if lang == "fr":
        dicoLogs = dicoFrLogs
        base = 1457835 # nb d'entrées au 28 août 2021
    elif lang == "en":
        dicoLogs = dicoEnLogs
        base = 155019 # nb d'entrées au 28 août 2021
    elif lang == "zh":
        dicoLogs = dicoZhLogs
        base = 188996 # nb d'entrées au 28 août 2021

    query = dicoLogs.find()
    nbDel=0
    nbAdd=0
    for q in query:
        if q['type'] == 'ADD': nbAdd+=1
        if q['type'] == 'DEL': nbDel+=1
    return base+nbAdd-nbDel

##### MISE EN MÉMOIRE DES DICTIONNAIRES (seulement les transcriptions+regions)
def makeWord2trans(alemDic):
    query = alemDic.find()#.limit(10000)
    word2trans = {}
    cpt = 0
    for q in query:
        if len(q['w'].split(' '))==1:
            # on met que les mots simples en mémoire (pas les expressions (=avec une espace))
            if q['w'] not in word2trans.keys():
                cpt+=1
                # word2trans[q['w']] = { "t" : q['t'] } # pas besoin de mettre en mémoire le reste pour l'instant (catégories, traductions, fantizi...)
                qt = []
                for t in q['t']:
                    qt.append({'tt':t['tt'].replace('.',''),'tr':t['tr']})
                word2trans[q['w']] = { "t" : qt }
                print("Mise en mémoire...",cpt,end='\r',flush=True)
            else:
                print('DOUBLON DÉTECTÉ :', q['w'])
    return word2trans


word2transFr = {}
word2transEn = {}
word2transZh = {}

print("Mise en mémoire de dicoFr...")
word2transFr = makeWord2trans(dicoFr)
print("Nombre d'entrées dans AlemDic-dicoFr :", getLenDic('fr'))

print("Mise en mémoire de dicoEn...")
word2transEn = makeWord2trans(dicoEn)
print("Nombre d'entrées dans AlemDic-dicoEn :", getLenDic(('en')))

print("Mise en mémoire de dicoZh...")
word2transZh = makeWord2trans(dicoZh)
print("Nombre d'entrées dans AlemDic-dicoZh :", getLenDic('zh'))

########################################
######### LECTURE DES FICHIERS #########
########################################

##### LECTURE DU CODE PHONEME-COULEUR #####
with open(phonColFile,"r") as phonFile:
    phon2class = json.load(phonFile)
    for i in phon2class.keys(): i = i.replace('(','').replace(')','')


##### LECTURE DES LISTES PHONEME-GRAPHIES (FIDEL) #####

## FR
with open(phonGraphFile,mode="r") as phonFile:
    phon2graphFr = {}
    phonCpt = 0
    graphCpt = 0

    for line in phonFile:
        phonCpt+=1
        line = line.strip()
        l= line.split(':')

        phon2graphFr[l[0]] = []

        listegraphies = l[1].split(',')
        for graph in listegraphies:
            phon2graphFr[l[0]].append(graph.replace("'","’"))
            graphCpt+=1

## EN
with open(phonGraphFileEn,mode="r") as phonFileEn:
    phon2graphEn = {}
    phonCptEn = 0
    graphCptEn = 0

    for line in phonFileEn:
        phonCptEn+=1
        line = line.strip()
        l= line.split(':')

        phon2graphEn[l[0]] = []

        listegraphies = l[1].split(',')
        for graph in listegraphies:
            phon2graphEn[l[0]].append(graph.replace("'","’"))
            graphCptEn+=1


##### LECTURE DES LOGS #####

def getLogStat(lang):
    if lang=="fr": alemDicLogs = dicoFrLogs
    if lang=="en": alemDicLogs = dicoEnLogs
    if lang=="zh": alemDicLogs = dicoZhLogs
    logStat = LogStat()
    logStat.cptEdit = 0
    logStat.cptModif = 0
    logStat.cptAdd = 0
    logStat.cptDel = 0
    for log in alemDicLogs.find():
        logStat.cptEdit+=1
        if log['type'] == "MODIF" : logStat.cptModif += 1
        if log['type'] == "ADD" : logStat.cptAdd += 1
        if log['type'] == "DEL" : logStat.cptDel += 1
    return logStat

print("Nombre de modifications du dictionnaire de français :", getLogStat('fr').cptEdit)
print("Nombre de modifications du dictionnaire d'anglais :", getLogStat('en').cptEdit)
print("Nombre de modifications du dictionnaire de mandarin :", getLogStat('zh').cptEdit)

##### SUPPLEMENTS POUR MANDARIN #####
with open(pinyin2apiFile) as inFile:
    pinyin2api = json.load(inFile)

with open(api2classFile) as inFile:
    api2class = json.load(inFile)

with open(pinyin2zhuyinFile) as inFile:
    pinyin2zhuyin = json.load(inFile)


########################################
######## CREATION DICTIONNAIRES ########
#### PHONO2ORTHOS (pour checkword) #####
########################################

def makePhono2orthos(dico):
    phono2orthos = {}
    for w,x in dico.items():
        for xt in x['t']:
            tt = xt['tt']
            if tt not in phono2orthos.keys():
                phono2orthos[tt] = []
            if w not in phono2orthos[tt]:
                phono2orthos[tt].append(w)
    return phono2orthos

print("Génération des dictionnaires phono2orthos pour checkword()...")
print("phono2orthosFr...")
phono2orthosFr = makePhono2orthos(word2transFr)
print("phono2orthosEn...")
phono2orthosEn = makePhono2orthos(word2transEn)
print("phono2orthosZh...")
phono2orthosZh = makePhono2orthos(word2transZh)
print("OK.")

########################################
######### LISTE DES FONCTIONS ##########
########################################

def traitement(mot, lang, liaison): # LIAISON : avec le caractere liaison en argument ('O', 'F', 'N' ou None)
    if lang == "fr":
        word2trans = word2transFr
        phon2graph = phon2graphFr
    elif lang == "en":
        word2trans = word2transEn
        phon2graph = phon2graphEn

    # Enregistrement de la casse
    caseMemory = []
    for i,lettre in enumerate(mot):
        if lettre.isupper():
            caseMemory.append(i)
            print('caseMemory +=',i,'(',lettre,')')
    mot = mot.lower()

    # Traitement de l'apostrophe
    if lang == "fr":
        mot = mot.replace("'",'’')
    if lang == "en":
        mot = mot.replace("’","'")
    
    # Si mot dans dictionnaire... decoupage(mot,transcription)
    if re.match(r'^\W+$|^\d+$',mot):
        print("'", mot, "' n'est pas un mot.")
        
        #verifier la liaison
        if '‿' in mot or "-͜" in mot :
            phon = phon_liaison(liaison[-2])
            if liaison[-1] == "O" :
                result = [([(phon, mot)], "", mot, "Liaison obligatoire")]
            elif liaison[-1] == "F" :
                result = [([(phon, mot)], "", mot, "Liaison facultative")]
            elif liaison[-1] == "N" :
                phon = "v"
                result = [([(phon, mot)], "", mot, "Liaison obligatoire")]
        else :
            result = [([('phon_neutre',mot)],"",mot,"")]

    elif mot in word2trans.keys():
        print("'", mot, "' trouvé dans le dico !",word2trans[mot])
        if lang in ["fr","en"]:
            transList = []
            shortmem = [] # pour pas afficher plusieurs fois un même alignement (avec et sans syllabation typiquement; le visuel est identique)
            for trans in word2trans[mot]['t']:
                newT = trans['tt']#.replace('.','')
                if newT not in shortmem:
                    transList.append((newT,trans['tr']))
                    shortmem.append(newT)
                else:
                    for r in trans['tr']:
                        # pour chaque région du trans courrant: si elle n'est pas dans les régions de la trans identique déjà présente dans transList: on l'ajoute
                        # ci-dessous : alreadyPushedTuple correspond au tuple (newT,trans['r']) déjà présent dans transList, auquel on veut ajouter r
                        if r not in [alreadyPushedTuple for alreadyPushedTuple in transList if alreadyPushedTuple[0]==newT][0][1]:
                            [alreadyPushedTuple for alreadyPushedTuple in transList if alreadyPushedTuple[0]==newT][0][1].append(r)
                print("TRANS",trans)
        result = []

        ############ partie d'appel de la fonction denasalisation
        if liaison != None :
            liste = ["aucun", "bien", "en", "on", "rien", "un", "non", "mon", "ton", "son","commun"]
            if mot[-1] == 'n' and mot not in liste :
                for i , trans in enumerate(transList):
                    trans = denasal(trans)
                    transList[i] = trans
        ############################################################

        for trans in transList:
            if lang == "fr":
                res = decoupage(mot,trans[0],phon2graph,phon2class) # add ,True to get live log
                ll = trans[1]
                tt = trans[0]
                msg = ""
            elif lang == "en":
                mot = mot.replace("'",'’')
                res, msg = decoupageEn(mot,trans[0],phon2graph,phon2class) # add ,True to get live log
                ll = trans[1]
                tt = trans[0]
            result.append((res,ll,tt,msg))
            
    else:
        print("'", mot, "' non trouvé !")
        result = [([('phon_inconnu',mot)],"","","Mot non trouvé dans le dictionnaire")]

    # Rétablissement de la casse
    for r in result:
        if len(r[0])>0:
            for m in caseMemory:
                cptlettre = 0
                while cptlettre < len(mot):
                    for k,tupl in enumerate(r[0]):
                        #print("\t",tupl)
                        cased = ''
                        for l in tupl[1]:
                            cased += l.upper() if cptlettre == m else l
                            cptlettre += 1
                        r[0][k] = (tupl[0],cased)
    return result



def traitementzh(mot):
    result = [] # liste type : [[car, api, phonlist, ton], [car, api, phonlist, ton]...]

    if mot in word2transZh.keys():
        pinyinOutput = word2transZh[mot]["t"][0]["tt"].lower()
        print("Mot trouvé dans le dictionnaire :", mot, pinyinOutput)

    else:
        print("Mot non trouvé dans le dictionnaire!")
        pinyinOutput = pinyin.get(mot, format="numerical", delimiter=" ")
        print("Translittération automatique :", pinyinOutput) # ni3 hao3

    pinparse = pinyinOutput.replace('v','ü').split(' ') # ['ni3', 'hao3']

    for hanzindex, pintone in enumerate(pinparse):
        if pintone[-1] in ['1', '2', '3', '4', '5'] and len(pintone)>1: # Si il y a un ton et que ce n'est pas qu'un chiffre, c'est que la pinyinisation a fonctionné
            res = [mot[hanzindex]]
            for el in pinyin2phon(pintone, pinyin2api, api2class):
                res.append(el)
            result.append(res)
        else:
            result.append((mot[hanzindex], "", [], 0, {}))

    print(result)
    return result


# EN COURS DE DÉV : permettre différentes phono pour un même mot zh
def traitementzh2(mot):
    word2trans = word2transZh
    result = [] # liste type : [[car, api, phonlist, ton], [car, api, phonlist, ton]...]
    pinyinOutputTransList = []

    # FIRST: Get pinyin transcription from dictionary or automatically
    if mot in word2trans.keys():
        print("'", mot, "' trouvé dans le dico !",word2trans[mot])
        shortmem = [] # pour pas afficher plusieurs fois un même alignement (avec et sans syllabation typiquement; le visuel est identique)
        for trans in word2trans[mot]['t']:
            newT = trans['tt']
            if newT not in shortmem:
                pinyinOutputTransList.append((newT,trans['tr']))
                shortmem.append(newT)
            print("TRANS",trans)
    else: 
        print("Mot non trouvé dans le dictionnaire!")
        pinyinOutputTransList.append(pinyin.get(mot, format="numerical", delimiter=" "))
        print("Translittération automatique :", pinyinOutputTransList) # ni3 hao3


    for pinyinOutput in pinyinOutputTransList:
        pinparse = pinyinOutput[0].replace('v','ü').split(' ') # ['ni3', 'hao3']

        for hanzindex, pintone in enumerate(pinparse):
            if pintone[-1] in ['1', '2', '3', '4', '5'] and len(pintone)>1: # Si il y a un ton et que ce n'est pas qu'un chiffre, c'est que la pinyinisation a fonctionné
                res = [mot[hanzindex]]
                for el in pinyin2phon2(pintone, pinyin2api, api2class, pinyin2zhuyin, True):
                    res.append(el)
                result.append(res)
            else:
                result.append((mot[hanzindex], "", [], 0, {}))
            
    print(result)
    return result


def getEntryByWord(m,mc,t,tc,lang):
    # m = mot (contenu de la barre de recherche "mot"),
    # mc = motCond (condition de recherche : contient, est égal à, commence par, finit par),
    # t = trans (contenu de la barre de recherche "transcription phonétique"),
    # tc = transCond (condition de recherche : contient, est égal à, commence par, finit par),
    # lang = langue cible ('fr', 'en', 'zh'...)

    if lang == "fr":
        m = m.replace("'",'’')
        dico = dicoFr
    elif lang == "en":
        m = m.replace('’',"'")
        dico = dicoEn
    elif lang == "zh":
        dico = dicoZh

    m = m.lower()
    t = t.replace('.','')

    if len(m) == 0: m = r'.*'
    if len(t) == 0: t = r'.*'

    result = {}

    mc1 = r'.*'
    mc2 = r'.*'
    if mc == 'startsBy' or mc == 'equalsTo':
        mc1 = r'^'
    if mc == 'endsBy' or mc == 'equalsTo':
        mc2 = r'$'
    
    tc1 = r'.*'
    tc2 = r'.*'
    if tc == 'startsBy' or tc == 'equalsTo':
        tc1 = r'^'
    if tc == 'endsBy' or tc == 'equalsTo':
        tc2 = r'$'

    # for entree, infos in word2trans.items():
    #     if re.match(mc1+m+mc2,entree):
    #         transOK = False
    #         for i,trans in enumerate(infos['t']):
    #             if re.match(tc1+t+tc2,trans['t'].replace('.','')):
    #                 transOK = True
    #         if transOK: result[entree] = infos['t']

    # Requêter directement MongoDB et envoyer l'ensemble des résultats (catégories, traductions... compris)
    limite = 999999 # limite à 999999
    cpt = 0
    for entree in dico.find({'w': {"$regex": mc1+m+mc2}}):
        transOK = False
        if len(t)>0:
            for i,trans in enumerate(entree['t']):
                if re.match(tc1+t+tc2,trans['tt'].replace('.','')):
                    transOK = True
        else:
            transOK = True
        if transOK:
            cpt+=1
            if cpt<=limite:
                result[entree['w']] = {}
                for i, j in entree.items():
                    if i != "_id":
                        result[entree['w']][i] = j
    return sorted(result.items(), key=lambda M: locale.strxfrm(M[0])) # mise dans l'ordre alphabétique français (ex. é après e, et pas après z)

def checkIfWordExists(w,t,lang='fr'):
    # Pour le Phonographe, renvoie objet : 'w':True si mot existe, 't':True si transcription existe, 'wt':True si ce mot avec cette trans existe 
    if lang == "fr":
        dico = word2transFr
        phono2ortho = phono2orthosFr
    elif lang == "en":
        w = w.replace('’',"'")
        dico = word2transEn
        phono2ortho = phono2orthosEn
    elif lang == "zh":
        dico = word2transZh
        phono2ortho = phono2orthosZh
    
    w = w.lower()
    res= {
        'w':False,
        't':False,
        'wt':False
    }


    if len(w)>0 and w in dico.keys():
        res['w'] = True
    if len(t)>0 and t in phono2ortho.keys():
        res['t'] = True
        if len(w)>0 and w in phono2ortho[t]:
            res['wt'] = True

    # if len(w)>0 and len(t)>0:
    #     for mot,transs in dico.items():
    #         if mot == w:
    #             res['w']=True;
    #             for trans in transs['t']:
    #                 if trans['tt'] == t:
    #                     res['wt']=True
    #                     res['t']=True
    #                     break
    #         for trans in transs['t']:
    #             if trans['tt'] == t:
    #                 res['t']=True
    #                 if res['w']: break
            
    # if len(w)==0 and len(t)>0:
    #     for mot,transs in dico.items():
    #         for trans in transs['t']:
    #             if trans['tt'] == t:
    #                 res['t']=True
    #                 break

    # if len(w)>0 and len(t)==0:
    #     for mot,transs in dico.items():
    #         if mot == w:
    #             res['w']=True;
    #             break
    
    return res


def modifEntry(user,mot,infos,lang):
    if lang == "fr": 
        word2trans = word2transFr
        dico = dicoFr
        phono2ortho = phono2orthosFr
    if lang == "en": 
        word2trans = word2transEn
        dico = dicoEn
        phono2ortho = phono2orthosEn
    if lang == "zh": 
        word2trans = word2transZh
        dico = dicoZh
        phono2ortho = phono2orthosZh

    oldInfos = dico.find_one({'w':mot})
    
    dico.update_one({"w":mot}, {"$set": infos})
    
    word2trans[mot]['t'] = []
    newTranss = []
    for x in infos['t']:
        word2trans[mot]['t'].append({'tt':x['tt'].replace('.',''), 'tr':x['tr']})

        # mise à jour de phono2ortho (1)
        # ajout de phono si n'existe pas encore
        if x['tt'].replace('.','') not in phono2ortho.keys():
            phono2ortho[x['tt'].replace('.','')] = [ mot ]
        # liste des transs pour identification des transs potentiellement supprimées
        newTranss.append(x['tt'])
    
    # mise à jour de phono2ortho (2)
    # suppression du mot pour la phono correspondante si phono modifiée ou supprimée
    for x in oldInfos['t']:
        if x['tt'].replace('.','') not in newTranss:
            # dans ce cas x['tt'] a été supprimée (ou modifiée), il faut donc la supprimer de phono2ortho
            if x['tt'].replace('.','') in phono2ortho.keys() and mot in phono2ortho[x['tt'].replace('.','')]:
                phono2ortho[x['tt'].replace('.','')].remove(mot)
                if len(phono2ortho[x['tt'].replace('.','')])==0:
                    phono2ortho.pop(x['tt'].replace('.',''), None)


    newlog = writeLog(Type="MODIF", user=user, mot=mot, oldMot=oldInfos, newMot=infos, lang=lang)
    print("Modification du dictionnaire par",user,": lang =",lang,"; mot =",mot,"; oldMot =",oldInfos,"; newMot =",infos)
    print("Newlog:",newlog)
    return {"msg":"Modification effectuée avec succès !", "newlog":newlog}


def addEntry(user,mot,trans,lang):
    if lang == "fr": 
        word2trans = word2transFr
        dico = dicoFr
        trans = trans.replace('g','ɡ')
        trans = trans.replace('r','ʁ')
        phono2ortho = phono2orthosFr
    if lang == "en": 
        word2trans = word2transEn
        dico = dicoEn
        trans = trans.replace('g','ɡ')
        phono2ortho = phono2orthosEn
    if lang == "zh": 
        word2trans = word2transZh
        dico = dicoZh
        phono2ortho = phono2orthosZh

    mot = mot.lower()
    mot = mot.strip()
    transList = trans.split(',')

    for ind,i in enumerate(transList):
        transList[ind] = i.strip()

    # update du dictionnaire en mémoire
    if mot not in word2trans.keys():
        newMot = {'t': []}
        for t in transList:
            if len(t)>0:
                if lang =="fr":
                    newMot['t'].append({ 'tt':t.replace('.',''), 'tr':['FR']})
                elif lang =="zh":
                    newMot['t'].append({ 'tt':t.replace('.',''), 'tr':['CN']})
                else:
                    newMot['t'].append({ 'tt':t.replace('.',''), 'tr':[]})

        if len(newMot['t'])>0:
            word2trans[mot] = newMot
            dico.insert_one({ 'w':mot, 't':newMot['t']})

            # Ajout de la phono dans phono2ortho
            for i in transList:
                if i not in phono2ortho.keys():
                    phono2ortho[i] = [ mot ]
                elif mot not in phono2ortho[i]:
                    phono2ortho[i].append(mot)

            newlog = writeLog(Type="ADD", user=user, mot=mot, oldMot="", newMot=transList, lang=lang)
            return {"msg":"Ajout effectué avec succès ! Merci pour votre participation !", "newlog":newlog}
        else:
            return {"msg":"Transcription vide. Veuillez indiquer au moins une transcriptions phonétiques. Ajout annulé."}
    else:
        return {"msg":"Ce mot est déjà dans le dictionnaire !"}

def delEntry(user,mot,lang):
    if lang == "fr": 
        word2trans = word2transFr
        dico = dicoFr
        phono2ortho = phono2orthosFr
    if lang == "en": 
        word2trans = word2transEn
        dico = dicoEn
        phono2ortho = phono2orthosEn
    if lang == "zh": 
        word2trans = word2transZh
        dico = dicoZh
        phono2ortho = phono2orthosZh
    
    if mot in word2trans.keys():
        # Suppression de la phono dans phono2ortho
        for x in word2trans[mot]['t']:
            if x['tt'].replace('.','') in phono2ortho.keys() and mot in phono2ortho[x['tt'].replace('.','')]:
                phono2ortho[x['tt'].replace('.','')].remove(mot)
                if len(phono2ortho[x['tt'].replace('.','')])==0:
                    phono2ortho.pop(x['tt'].replace('.',''), None)

        oldmot = word2trans[mot]
        word2trans.pop(mot)
        dico.delete_one({'w':mot})

        newlog = writeLog(Type="DEL", user=user, mot=mot, oldMot=oldmot, newMot="",lang=lang)
        print("Modification du dictionnaire par",user,": suppression de mot =",mot)
    
        return {"msg":"Suppression effectuée avec succès.", "newlog":newlog}


def sendMeLogPlease(lang):
    if lang == "fr":
        logDic = dicoFrLogs
    elif lang == "en":
        logDic = dicoEnLogs
    elif lang == "zh":
        logDic = dicoZhLogs
    
    rep = []
    for log in logDic.find():
        rep.append(noIdDict(log))

    return rep

def noIdDict(dic):
    if type(dic) is dict:
        # renvoit un dic sans la clé "_id"
        return { i:j for i,j in dic.items() if i!='_id' }
    else:
        return dic

def writeLog(Type,user,mot,oldMot,newMot,lang):
    if lang == "fr":
        logDic = dicoFrLogs
    elif lang == "en":
        logDic = dicoEnLogs
    elif lang == "zh":
        logDic = dicoZhLogs

    newlog = {
        "date":datetime.datetime.now(),
        "user":user,
        "word":mot,
        "old":noIdDict(oldMot),
        "new":noIdDict(newMot),
        "type":Type
    }
    logDic.insert_one(newlog)
    
    print("New log: lang={} ; user={} ; word={} ; type={}".format(lang,user,mot,Type))
    return noIdDict(newlog)

def addStat(app, module, ip, agent, country, lang):
    newStat = {
        'date':datetime.datetime.now(),
        'app': app,
        'module': module,
        'ip': ip,
        'agent': str(agent),
        'country': country,
        'lang': lang
    }
    users_appstats.insert_one(newStat)