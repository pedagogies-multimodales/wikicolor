# The words in the list below should be considered as one word instead of two : gon na → gonna

def getOneWordList():
    return {
                "gon":"na",
                "got":"ta",
                "can":"not",
                "do":"n't",
                "does":"n't",
                "did":"n't",
                "ca":"n't",
                "could":"n't",
                "should":"n't",
                "is":"n't",
                "are":"n't",
                "would":"n't",
                "wo":"n't",
                "have":"n't",
                "had":"n't",
                "were":"n't",
                "ai":"n't",
                "has":"n't",
                "must":"n't",
                "sha":"n't",
                "was":"n't",
                "might":"n't",
                "may":"n't"
            }

def getNounVerbStressList():
    liste = []
    with open('colorapp/stressPattern.txt') as inf:
        for line in inf:
            line = line.strip()
            liste.append(line)
    return liste