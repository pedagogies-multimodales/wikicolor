from django.db import models

# Create your models here.
class Phonograph:
    phon: str
    graph: str

class Data:
    text: str
    pphh: list
    update: str
    colorType: str
    defaultBg: str
    lenDic: int
    lenModifDic: int

class Entree:
    mot: str
    trans: str
    motcond: str
    transcond: str
    result: list
    nb: int
    message: str
    update: str

class LogStat:
    cptEdit: int
    cptModif: int
    cptAdd: int
    cptDel: int

class DicEntry:
    mot: models.CharField(max_length=100)
    url: models.CharField(max_length=150)
    trans: str

class commitData:
    update: str
    listCommits: str