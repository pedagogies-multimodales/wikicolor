from django.shortcuts import render, HttpResponseRedirect
from . import textPhonographer as txtphono
from .models import Phonograph, Data, Entree, commitData
from django.http import JsonResponse
from .liaisons import *
from .english_spacyExceptions import *
import json, spacy, subprocess, re, requests, os
from user_agents import parse

print('Chargement des modèles de langue...')
print('fr_core_news_sm...')
nlpFr = spacy.load('fr_core_news_sm')
print('en_core_web_sm...')
nlpEn = spacy.load("en_core_web_sm")
print('zh_core_web_sm...')
nlpZh = spacy.load("zh_core_web_sm")
print('OK.')
logFile = "../logs/dico_frwiktionary-20200301_v2.log"


## RÉCUPÉRATION DE LA LISTE DES EXCEPTIONS POUR L'ANGLAIS
asOneWord = getOneWordList()
nounVerbStressList = getNounVerbStressList()

def redirApp(request):
    return HttpResponseRedirect('/')

def main0(request):
    data = Data()
    data.update = updateTime()
    return render(request, 'coloriseur.html', {'data': data, 'pageLang':"fr"})

def main(request,pageLang,**mySettings):
    data = Data()
    data.update = updateTime()
    return render(request, 'coloriseur.html', {'data': data, 'pageLang':pageLang})

def colorize(request):
    colis = json.loads(request.body)
    text = colis['inText']
    lang = colis['lang']
    if lang == "fr":
        nlpText = nlpFr(text)
    elif lang == "en":
        nlpText = nlpEn(text)
    elif lang == "zh":
        nlpText = nlpZh(text)
    

    ######### traitement du tiret ############
    if lang == "fr":
        listeMots = []

        for i, mot in enumerate(nlpText):
            # Traiter les mots qui contiennent le '-'.
            # Ex : "-il", "Peut-être"
            if "-" in mot.text and len(mot.text) > 1 :
                temp = ""
                for l in mot.text :
                    if l != "-":
                        temp += l
                    else:
                        if temp == '':
                            listeMots.append("-")
                        else:
                            listeMots.append(temp)
                            listeMots.append("-")
                            temp = ""
                if temp != "":
                    listeMots.append(temp)
            # Traiter uniquement le tiret
            else:
                listeMots.append(mot.text)

        tiret = {} # EX : Vas-y ! Pouvait-il partir ? ==> {'Vas': 1, 'Pouvait': 5}
        for k,item in enumerate(listeMots):
            if item == "-" and k > 0 :
                tiret[listeMots[k-1]] = k
        print(tiret)

        # Remplacer tous les tirets dans le texte par le caractere espace
        if "-" in listeMots:
            text = text.replace('-', ' ')
        
        nlpText = nlpFr(text)
    #########################################
    
    outText = []

    # appeler la fonction pour savoir s'il y a une locution
    index = liaison_locution(text, nlpText)
    print("Index", index)

    for j, token in enumerate(nlpText):
        sdl = re.findall(r'\r\n',token.text)
        print("sdl =",sdl)
        if len(sdl) > 0:
            for s in sdl:
                print("Saut de ligne.")
                outText.append('§')
        else:

            thisTokenText = token.text

            # TRAITEMENT CONTRACTIONS ANGLAIS AUTRES gonna cannot gotta
            if lang == "en":
                if thisTokenText.lower() in asOneWord.keys() and nlpText[j+1].text.lower() == asOneWord[thisTokenText.lower()]:
                    print("CONCATÉNATION CONTRACTION ANGLAIS:", thisTokenText, nlpText[j+1].text)
                    thisTokenText = thisTokenText + nlpText[j+1].text
                elif thisTokenText.lower() in asOneWord.values() and nlpText[j-1].text.lower() in asOneWord.keys() and asOneWord[nlpText[j-1].text.lower()] == thisTokenText.lower():
                    continue # => si on est passé par le if à l'itération suivante, on passe au token suivant directement (ignorer "n't"..etc)


            print("Mot en entrée :",thisTokenText)
            if lang == "fr" or lang == "en":
                liaison = None

                ## TRAITEMENT LIAISON POUR FR
                if lang == "fr":
                    if j + 1 < len(nlpText):
                        # ici on met la liaison directement s'il s'agit d'une locution avec exception
                        if index != None and (index[0]+index[1]) == j:
                                bigram = (nlpText[j].text, nlpText[j+1].text)
                                liaison = index[2]
                                print("bigram de locution :",bigram)
                        # appel de la fonction obtenirM1M2
                        else:
                            bigram = obtenirM1M2(thisTokenText, nlpText[j+1].text)
                            print("bigram normal :",bigram)
                            # vérifie les règles de liaisons et denasalisation
                            if bigram != None:
                                print("bigram normal :",token.pos_,nlpText[j+1].pos_)
                                liaison = verifliaison(token, nlpText[j+1])
                                print("type de liaison : ",liaison)

                result = txtphono.traitement(thisTokenText,lang,liaison) # LIAISON : ajoute le booléen liaison en argument

                phonographieList = []
                for r in result:
                    phonographie = []
                    for i in r[0]:
                        ph = {}
                        ph['phon'] = i[0]
                        ph['graph'] = i[1]
                        phonographie.append(ph)
                    phonographieList.append((phonographie,r[1],r[2],r[3]))

                ## EXCEPTION ANGLAIS: Stress au début si NOUN/ADJ, fin si VERB →liste des mots dans ce cas dans nounVerbStressList
                if lang == "en" and thisTokenText in nounVerbStressList:
                    print("STRESS POSITIONING:",token.pos_)
                    for w in phonographieList:
                        print(w[2],len(re.search(".*ˈ",w[2]).group()))
                    if token.pos_ == "VERB":
                        phonographieList.sort(key=lambda w:  len(re.search(".*ˈ",w[2]).group()), reverse=True)
                    else:
                        phonographieList.sort(key=lambda w:  len(re.search(".*ˈ",w[2]).group()))
                    for w in phonographieList:
                        print(w[2])
                #####

                outText.append(phonographieList)


                ## SUITE LIAISON ##
                if lang == 'fr':
                    ## Affichage de la liaison hors du mot
                    juge = False
                    if len(tiret) > 0 and thisTokenText in tiret.keys() :
                        if liaison != None :
                            result1 = txtphono.traitement("-͜", lang, thisTokenText+liaison)
                            juge = True
                        elif liaison == None :
                            result1 = txtphono.traitement("-", lang, liaison)
                            juge = True
                    else:
                        if liaison != None:
                            result1 = txtphono.traitement("‿", lang, thisTokenText+liaison)
                            juge = True
                    if juge == True :
                        phonographieList = []
                        for r in result1:
                            phonographie = []
                            for i in r[0]:
                                ph = {}
                                ph['phon'] = i[0]
                                ph['graph'] = i[1]
                                phonographie.append(ph)
                            phonographieList.append((phonographie, r[1], r[2], r[3]))
                        outText.append(phonographieList)

            elif lang == "zh":
                result = txtphono.traitementzh(thisTokenText)
                outText.append(result)
            #print("Résultat en sortie :", result)
               
    rep = {
        'outText': outText
    }
    return JsonResponse(rep)


def getPhonoOf(request):
    # Renvoie une liste(texte) de listes(phonographiesPossibles) de listes(phonographie) de phonèmes à partir d'un mot ou d'une expression
    # ex. input="chat" → output=[[[phon_s_maj, phon_a], [phon_ts_maj, phon_a, phon_t]]]
    # ex. input="le chat" → output= [
    #   [
    #       [phon_l,phon_2],
    #       [phon_e_maj,phon_l,phon_2]
    #   ],
    #   [
    #       [phon_s_maj, phon_a],
    #       [phon_ts_maj, phon_a, phon_t]
    #   ]
    # ]
     
    colis = json.loads(request.body)
    text = colis['inText']
    lang = colis['lang']
    if lang == "fr":
        nlpText = nlpFr(text)
    elif lang == "en":
        nlpText = nlpEn(text)
    outPhono = []

    for token in nlpText:
        sdl = re.findall(r'\r\n',token.text)
        print("sdl =",sdl)
        if len(sdl) > 0:
            for s in sdl:
                print("Saut de ligne.")
                outText.append('§')
        else:
            print("Mot en entrée :",token.text)
            if lang == "en":
                result = txtphono.traitement(token.text,lang,None)
            else:
                result = txtphono.traitement(token.text,lang,None)
            print(result)
        
            phonographieList = []
            for r in result:      
                phonoliste = []
                for i in r[0]:
                    phonoliste.append(i[0])
                    
                phonographieList.append(phonoliste)
            outPhono.append(phonographieList)
    rep = {
        'outText': outPhono
    }
    return JsonResponse(rep)

def getPhonoOfGETrequest(request):
    # Renvoie une liste(texte) de listes(phonographiesPossibles) de listes(phonographie) de phonèmes à partir d'un mot ou d'une expression
    # ex. input="chat" → output=[[[phon_s_maj, phon_a], [phon_ts_maj, phon_a, phon_t]]]
    # ex. input="le chat" → output= [
    #   [
    #       [phon_l,phon_2],
    #       [phon_e_maj,phon_l,phon_2]
    #   ],
    #   [
    #       [phon_s_maj, phon_a],
    #       [phon_ts_maj, phon_a, phon_t]
    #   ]
    # ]
     
    text = request.GET.get('text', None)
    lang = request.GET.get('lang', None)
    if lang == "fr":
        nlpText = nlpFr(text)
    elif lang == "en":
        nlpText = nlpEn(text)
    outPhono = []

    for token in nlpText:
        sdl = re.findall(r'\r\n',token.text)
        print("sdl =",sdl)
        if len(sdl) > 0:
            for s in sdl:
                print("Saut de ligne.")
                outText.append('§')
        else:
            print("Mot en entrée :",token.text)
            if lang == "en":
                result = txtphono.traitement(token.text,lang,None)
            else:
                result = txtphono.traitement(token.text,lang,None)
            print(result)
        
            phonographieList = []
            for r in result:      
                phonoliste = []
                for i in r[0]:
                    phonoliste.append(i[0])
                    
                phonographieList.append(phonoliste)
            outPhono.append(phonographieList)
    rep = {
        'outText': outPhono
    }
    return JsonResponse(rep)

# def updateTime():
#     upd = str(subprocess.check_output(["git", "log", "-1", "--format=%cd", "--date=short"]))
#     #ver = str(subprocess.check_output(["git", "rev-list", "--all", "--count"]))
#     return 'Version 5.0 (mis à jour le '+upd[2:-3]+')'

def get_git_date():
    cache_file = '/tmp/git_date_cache.txt'
    if os.path.exists(cache_file):
        with open(cache_file, 'r') as f:
            return f.read().strip()
    else:
        git_date = subprocess.check_output(["git", "log", "-1", "--format=%cd", "--date=short"]).decode('utf-8').strip()
        with open(cache_file, 'w') as f:
            f.write(git_date)
        return git_date

def updateTime():
    return "(Last update: "+str(get_git_date())+")"


def dicoView(request):
    print("Réception requête dicoView().")
    data = Entree()
    data.update = updateTime()
    data.nbMotsFr = txtphono.getLenDic('fr')
    data.nbMotsEn = txtphono.getLenDic('en')
    data.nbMotsZh = txtphono.getLenDic('zh')
    return render(request, 'editDicoHome.html', {'data': data})

def dicoViewFr(request):
    data = Entree()
    data.update = updateTime()
    data.lenDic = txtphono.getLenDic('fr')
    data.logStat = txtphono.getLogStat('fr')
    data.dicoLang = "fr"
    return render(request, 'editDico.html', {'data': data})

def dicoViewEn(request):
    data = Entree()
    data.update = updateTime()
    data.lenDic = txtphono.getLenDic('en')
    data.logStat = txtphono.getLogStat('en')
    data.dicoLang = "en"
    return render(request, 'editDico.html', {'data': data})

def dicoViewZh(request):
    data = Entree()
    data.update = updateTime()
    data.lenDic = txtphono.getLenDic('zh')
    data.logStat = txtphono.getLogStat('zh')
    data.dicoLang = "zh"
    return render(request, 'editDico.html', {'data': data})

def dicoReq(request):
    dicoLang = request.GET.get('dicoLang', None)
    mot = request.GET.get('mot', None)
    trans = request.GET.get('trans', None)
    motCond = request.GET.get('motCond', None)
    transCond = request.GET.get('transCond', None)
    
    rep = {
        "listeEntrees" : txtphono.getEntryByWord(m=mot,mc=motCond,t=trans,tc=transCond,lang=dicoLang)
    }
    return JsonResponse(rep)

def checkWord(request):
    w = request.GET.get('w', None)
    t = request.GET.get('t', None)
    lang = request.GET.get('lang', None)

    rep = {
        'rep': txtphono.checkIfWordExists(w,t,lang)
    }
    return JsonResponse(rep)

def checkWordList(request):
    colis = json.loads(request.body)
    wordPhonoSpansList = colis['wordPhonoSpansList']
    lang = colis['lang']

    repList = []

    for w in wordPhonoSpansList:
        repList.append(txtphono.checkIfWordExists(w['w'],w['t'],lang))

    rep = {
        'rep': repList
    }
    return JsonResponse(rep)

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def editEntry(request):
    colis = json.loads(request.body)
    mot = colis['mot']
    lang = colis['lang']
    infos = colis['infos']
    print(infos)
    ipCli = get_client_ip(request)

    return JsonResponse(txtphono.modifEntry(ipCli,mot,infos,lang))

def newEntry(request):
    mot = request.GET.get('mot', None)
    trans = request.GET.get('trans', None)
    lang = request.GET.get('lang', None)
    ipCli = get_client_ip(request)

    return JsonResponse(txtphono.addEntry(ipCli,mot,trans,lang))

def supprEntry(request):
    mot = request.GET.get('mot', None)
    lang = request.GET.get('lang', None)
    ipCli = get_client_ip(request)

    return JsonResponse(txtphono.delEntry(ipCli,mot,lang))

def getLog(request):
    lang = request.GET.get('lang', None)
    rep = txtphono.sendMeLogPlease(lang)
    return JsonResponse({"logs":rep})

def addStat(request):
    if get_client_ip(request)!="127.0.0.1":
        colis = json.loads(request.body)
        app = colis["app"]
        module = colis["module"]
        ip = get_client_ip(request)
        agent = parse(request.META['HTTP_USER_AGENT'])
        country = getCountry(get_client_ip(request))
        lang = colis["lang"]

        txtphono.addStat(app, module, ip, agent, country, lang)

    return JsonResponse({})

def getCountry(ip):
    endpoint = f'https://ipinfo.io/{ip}/json'
    response = requests.get(endpoint, verify = True)

    if response.status_code != 200:
        return 'Status:', response.status_code, 'Problem with the request. Exiting.'
        exit()

    data = response.json()
    print(data)
    return data['country']