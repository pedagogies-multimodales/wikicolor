
var dicoTok = {};
var dicoId = {};

async function getColorisation() {
	var inText = document.getElementById('inText').value;
	var lang = document.getElementById('choixLang').value;
    
    if (inText.length>0) {
        document.getElementById('loader').style.display = "block";
        // ON EMBALLE TOUT ÇA
        var colis = {
            inText: inText,
            lang: lang
        }
        // console.log('Envoi colis:',colis);

        // Paramètres d'envoi
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(colis)
        };
        
        // ENVOI
        const response = await fetch('/colorize/', options)
        const data = await response.json();
        console.log(data);
        
        // ÉCRITURE DU RÉSULTATS DANS DIV RÉSULTATS
        if (lang == "fr" || lang == "en"){
            document.getElementById('outputzh').innerHTML = "";
            var outputDiv = document.getElementById('output');
            var outText = data['outText'];
            outputDiv.innerHTML = "";
            dicoTok = {};
            dicoId = {};
            var noSpace = false;

            for (i=0; i<outText.length; i++) {

                // REMPLISSAGE DES ZONES MOTS DANS DIV RÉSULTATS
                if (outText[i][0][0] == []) {
                    // Sécurité: si phonigraphie est une liste vide, on tente quand même d'afficher du texte en gris
                    console.log("Bug outText[",i,"][0][0] is empty!");

                } else if (outText[i][0][0][0] == undefined){
                    console.log("undefined: index",i,outText[i][0][0])
                } else {
                    if (outText[i][0][0][0].graph.match(/\n+/)){
                        for (h=0; h<outText[i][0][0][0].graph.length; h++) {
                            outputDiv.innerHTML = outputDiv.innerHTML + '<br>';
                        }
                    } else if (outText[i][0][0][0].graph.match(/^‿|-͜$/)){
                        if(outText[i][0][3] == "Liaison obligatoire") {
                            classliaison = "liaisonObligatoire"
                        } else if (outText[i][0][3] == "Liaison facultative") {
                            classliaison = "liaisonFacultative"
                        }
                        outputDiv.innerHTML = outputDiv.innerHTML + '<span class="'+ classliaison +' phon '+ outText[i][0][0][0].phon +'" title="'+ outText[i][0][3] +'">'+ outText[i][0][0][0].graph +'</span>';
                        noSpace = true;
                    } else if (outText[i][0][0][0].graph.match(/^(,|\.|…|\)|\]|\}|%|>|»|”|-)$/)) {
                        outputDiv.innerHTML = outputDiv.innerHTML + '<span class="phon_neutre">'+ outText[i][0][0][0].graph +'</span>';
                        if (outText[i][0][0][0].graph.match(/^-$/)) noSpace = true; else noSpace = false;
                    } else if (outText[i][0][0][0].graph.match(/\(|\[|\{|<|«|“/)) {
                        outputDiv.innerHTML = outputDiv.innerHTML + '<span class="space"> </span><span class="phon_neutre">'+ outText[i][0][0][0].graph +'</span>';
                        noSpace = true;
                    } else if (noSpace) {
                        if (outText[i].length>1) {
                            outputDiv.innerHTML = outputDiv.innerHTML + '<span class="tokens" id="tok'+ i +'" onclick="showAlignPop(this.id)"></span>';
                        } else {
                            outputDiv.innerHTML = outputDiv.innerHTML + '<span class="tokens" id="tok'+ i +'"></span>';
                        }
                        if (outText[i][0][0][0].graph.match(/^.+’$/)) noSpace = true; else noSpace = false;
                    } else {
                        if (outText[i][0][0][0].graph.match(/^’.+$/)) { // I'll you're
                            if (outText[i].length>1) {
                                outputDiv.innerHTML = outputDiv.innerHTML + '<span class="tokens" id="tok'+ i +'" onclick="showAlignPop(this.id)"></span>';
                            } else {
                                outputDiv.innerHTML = outputDiv.innerHTML + '<span class="tokens" id="tok'+ i +'"></span>';
                            }
                        } else {
                            if (outText[i].length>1) {
                                outputDiv.innerHTML = outputDiv.innerHTML + '<span class="space"> </span><span class="tokens" id="tok'+ i +'" onclick="showAlignPop(this.id)"></span>';
                            } else {
                                outputDiv.innerHTML = outputDiv.innerHTML + '<span class="space"> </span><span class="tokens" id="tok'+ i +'"></span>';
                            }
                        }
                        if (outText[i][0][0][0].graph.match(/^.+’$/)) noSpace = true; else noSpace = false;
                    }
                
                
                    // FORMATAGE DES SPANS
                    if (outText[i][0][0][0].graph != '\n'){
                        dicoTok['tok'+i] = [];
                        waitinglist = []; // we will put all non aligned ones here, waiting for pushing them all at the end (so that they can't appear first)
                        prioritylist = []; // this will be for making UK pronunciations appearing first if thisRegion is set to UK (user choice)
                        otherTranslist = []; // for other spellings (other regions)
                        for (j=0; j<outText[i].length; j++) {
                            var newWord = '';
                            for (k=0; k<outText[i][j][0].length; k++) {
                                newWord = newWord + '<span class="phon '+ outText[i][j][0][k].phon +'">'+ outText[i][j][0][k].graph +'</span>';
                            }
                            if (outText[i][j][3].length == 0 && outText[i][j][0].length>0) {
                                var priorityCheck = false;
                                for (r=0; r<outText[i][j][1].length; r++) {
                                    // for each region, if thisRegion here, put this spelling in the prioritylist
                                    if (loc2stand[outText[i][j][1][r]]==thisRegion){ priorityCheck = true }
                                }
                                if (priorityCheck) {
                                    prioritylist.push([newWord,outText[i][j][1],outText[i][j][2],outText[i][j][3]]);
                                } else {
                                    otherTranslist.push([newWord,outText[i][j][1],outText[i][j][2],outText[i][j][3]]);
                                }
                            } else {
                                waitinglist.push([newWord,outText[i][j][1],outText[i][j][2],outText[i][j][3]]);
                            }
                        }
                        for (l=0; l<prioritylist.length; l++){
                            dicoTok['tok'+i].push(prioritylist[l]);
                        }
                        for (l=0; l<otherTranslist.length; l++){
                            dicoTok['tok'+i].push(otherTranslist[l]);
                        }
                        for (l=0; l<waitinglist.length; l++){
                            dicoTok['tok'+i].push(waitinglist[l]);
                        }
                    }

                    // INITIALISATION DU dicoId
                    if (outText[i][0][0][0].graph != '\n'){
                        dicoId['tok'+i] = 0;
                    }
                }
                document.getElementById('loader').style.display = "none";
            }

            var tokens = document.getElementsByClassName("tokens");
            for (i = 0; i < tokens.length; i++) {
                if (dicoTok[tokens[i].id].length > 1) {
                    tokens[i].innerHTML = dicoTok[tokens[i].id][0][0];
                    if (lang=="en") {
                        $('#'+tokens[i].id).addClass('transMultEn');
                        $('#'+tokens[i].id).prop('title', langJson['ti_multitrans']['en']);
                    } else {
                        $('#'+tokens[i].id).addClass('transMult');
                        $('#'+tokens[i].id).prop('title', langJson['ti_multitrans']['fr']);
                    }
                } else {
                    tokens[i].innerHTML = dicoTok[tokens[i].id][0][0];
                };
            };
            unknownMark();
            bugMark();
            toggleLiaisons()

            if (document.getElementById('bicolor').checked) {
                toBicolor();
            }
            if (document.getElementById('monochrome').checked) {
                toMonochrome();
            }
        } else if (lang == "zh") {
            document.getElementById('output').innerHTML = "";
            var outputDiv = document.getElementById('outputzh');
            var outText = data['outText'];
            outputDiv.innerHTML = "";

            var linediv = document.createElement('div');
            outputDiv.appendChild(linediv);

            var tones = {1:'˥',2:'˧˥',3:'˨˩˦',4:'˥˩',5:''};

            for (i=0; i<outText.length; i++) {
                // Pour chaque mot outText[i]
                var worddiv = document.createElement('div');
                worddiv.classList = "zhtoken";
                linediv.appendChild(worddiv);
                
                for (j=0; j<outText[i].length; j++) {
                    // Pour chaque hanzi outText[i][j]
                    var zi = outText[i][j][0];
                    var pinyin = outText[i][j][1];
                    var api = outText[i][j][2];
                    var phonList = outText[i][j][3];
                    var ton = outText[i][j][4];

                    if (zi == '\n'){
                        linediv = document.createElement('div');
                        outputDiv.appendChild(linediv);
                        var newbr = document.createElement('br');
                        outputDiv.appendChild(newbr);
                        linediv = document.createElement('div');
                        outputDiv.appendChild(linediv);
                    } else {
                        if (phonList.length>0){
                            var svg = createZh(zi,phonList.length,phonList,ton);
                            var svgdiv = document.createElement('div');
                            svgdiv.title = `${pinyin}${ton}\n${api}`+tones[ton];
                            svgdiv.appendChild(svg);
                            worddiv.appendChild(svgdiv);
                        } else {
                            // Le contenu n'est pas colorisé : on met en gris sans cadre
                            var newEl = document.createElement('span');
                            newEl.classList = "sinosvgNeutre";
                            newEl.innerHTML = zi;
                            worddiv.appendChild(newEl);
                        }
                    }
                }
            }
            document.getElementById('loader').style.display = "none";
        }
        addStat("coloriser");
    }
}

function delInText() {
	document.getElementById('inText').value = "";
	document.getElementById('output').innerHTML = "";
	document.getElementById('outputzh').innerHTML = "";
}

function unknownMark() {
	var marks = document.getElementsByClassName('unknownMark');
	var i;
	for (i=0; i < marks.length; i++) {
		//marks[i].parentNode.removeChild(marks[i]);
		marks[i].style.display = "none";
	}

	var unknowns = document.getElementsByClassName('phon_inconnu');
	var i;
	for (i = 0; i < unknowns.length; i++) {
		var newMark = document.createElement('span');
		newMark.classList = 'glyphicon glyphicon-edit unknownMark';
		newMark.title = langJson["ti_add2dico"][thisPageLang];
		newMark.value = unknowns[i].innerHTML;
        newMark.innerHTML = "?";
		newMark.onclick = function(){
			dicoAddPop(this.value);
		};
		unknowns[i].parentNode.insertBefore(newMark,unknowns[i].nextSibling);
	};
}

function dicoAddPop(mot) {
	document.getElementById('popAddwMot').innerHTML = "« "+mot+" »";
	document.getElementById('popAddwInputSpan').innerHTML = '<input id="popAddwTrans" class="popAddwTrans api-kb" type="text" value=""/>';
	var modalpopAddw = document.getElementById('popAddw');
	modalpopAddw.style.display = "block";
	getAPIkeyboard(thisPageLang);
	window.onclick = function(event) {
		if (event.target == modalpopAddw) {
			modalpopAddw.style.display = "none";
		}
	}
}

function dicoAddPopBtnAjouter() {
    var mot = document.getElementById('popAddwMot').innerHTML.replace('« ','').replace(' »','');
    var trans = document.getElementById('popAddwTrans').value;
    if (trans.length > 0) {
        validEntry(mot, trans);
    } else {
        window.alert(langJson['alert_addAtLeastOneTrans'][thisPageLang]);
    }
}

function validEntry(mot, trans) {
    document.getElementById('actionBlock').style.display = '';
    document.body.style.cursor = "wait";

    $.ajax({
        url: '/addEntry/',
        data: {
            mot: mot,
            trans: trans,
            lang: thisPageLang
        },
        dataType: 'json',
        success: function(data) {
            document.body.style.cursor = "default";
            document.getElementById('actionBlock').style.display = 'none';
            console.log(data.msg);
			window.alert(data.msg);
			document.getElementById('popAddw').style.display = 'none';
			getColorisation();
        },
        error: function(){
            document.body.style.cursor = "default";
            window.alert(data.msg);
            document.getElementById('actionBlock').style.display = 'none';
        }
    });
}

function bugMark() {
	var marks = document.getElementsByClassName('bugMark');
	var i;
	for (i=0; i < marks.length; i++) {
		//marks[i].parentNode.removeChild(marks[i]);
		marks[i].style.display = "none";
	}
	var bugs = document.getElementsByClassName('phon_echec');
	var i;
	for (i = 0; i < bugs.length; i++) {
		var newMark = document.createElement('span');
		newMark.classList = 'glyphicon glyphicon-flash bugMark';
        newMark.innerHTML = ' X';
		var thisMotId = bugs[i].parentNode.id;
		console.log(thisMotId);
		var infoMot = dicoTok[thisMotId][dicoId[thisMotId]];
		newMark.title = langJson['ti_bugMark'][thisPageLang] + ' /' + infoMot[2] + '/: ' + infoMot[3];
		bugs[i].parentNode.insertBefore(newMark,bugs[i].nextSibling);
	};
}



//réinitialise le bicolor (si bicolor)
if (document.getElementById('bicolor').checked == true) {
		toBicolor();
};


function showAlignPop(tok) {
    // Boîte de dialogue pour sélectionner un autre alignement phonographémique
	var alignPopDivBack = document.getElementById('alignPopDivBack');
	alignPopDivBack.style.display = "block";

	var alignPopDiv = document.createElement('div');
	var el = document.getElementById(tok);

	alignPopDiv.classList = "alignPopDiv";
	alignPopDiv.id = "alignPopDiv";
	alignPopDiv.style.top = el.offsetTop + el.offsetHeight +"px";
	alignPopDiv.style.left = el.offsetLeft - 20 +"px";
	alignPopDiv.style.minWidth = el.offsetWidth + "px";
	el.appendChild(alignPopDiv);	

	var tab = '<table class="tableAlign">';
	for (i=0; i<dicoTok[tok].length; i++) {
        var stand = "";
        var locOK = []; // in order to avoid putting the same flag twice
        for (loc in dicoTok[tok][i][1]){
            var locMeta = loc2stand[dicoTok[tok][i][1][loc]];
            if (!locOK.includes(locMeta) && loc2stand[dicoTok[tok][i][1][loc]]){
                stand += "<img style='height:25px; border:1px solid white; border-radius:3px; margin:5px;' src='static/im/flag-"+loc2stand[dicoTok[tok][i][1][loc]].toLowerCase()+".png' title='"+metaRegions[thisPageLang][loc2stand[dicoTok[tok][i][1][loc]]]+"'/>";
                locOK.push(locMeta);
            }
        }
		tab = tab + "<tr onmousedown='selectAlign(\""+ tok + "\"," + i +")' onmouseup='supprAlignPopDiv();' title='"+dicoTok[tok][i][1]+": /"+dicoTok[tok][i][2]+"/ "+dicoTok[tok][i][3]+"'><td>"+dicoTok[tok][i][0]+"</td><td>"+ stand +"</td></tr>";
	}
	tab = tab + "</table>";
	alignPopDiv.innerHTML = tab;
	window.onclick = function(event) {
		if (event.target == document.getElementById('alignPopDivBack')) {
			document.getElementById('alignPopDiv').parentElement.removeChild(document.getElementById('alignPopDiv'));
			document.getElementById('alignPopDivBack').style.display = "none";
		}
	}
}

function selectAlign(tok,i){
	dicoId[tok] = i;
	document.getElementById('alignPopDivBack').style.display = "none";
	document.getElementById(tok).innerHTML = dicoTok[tok][i][0];
	//réinitialise le bicolor (si bicolor)
	if (document.getElementById('bicolor').checked == true) {
		toBicolor(); // réinitialisation du bicolor (nécessaire si nb phonèmes différents)
	};
	if (document.getElementById('monochrome').checked == true) {
		toMonochrome(); // réinitialisation du monochrome
	};
	unknownMark();
	bugMark();
};

function supprAlignPopDiv() {
	document.getElementById('alignPopDiv').parentElement.removeChild(document.getElementById('alignPopDiv'));
}

function toSilentWay() {
	var phonList = document.getElementsByClassName("phon");
		for (var i = 0; i < phonList.length; i++) {
			phonList.item(i).classList.remove("monochrome");
			phonList.item(i).classList.remove("bicolor1");
			phonList.item(i).classList.remove("bicolor2");
		};
};

// POUR PASSER EN BICOLOR (se fait à partir du résultat aux couleurs SW)
// on ajoute la classe .bicolor1 ou .bicolor2 alternativement, si la graphie n'est pas .phon_neutre
function toBicolor() {
	var phonList = document.getElementsByClassName("phon");
		for (var i = 0; i < phonList.length; i++) {
			phonList.item(i).classList.remove("monochrome");
			phonList.item(i).classList.remove("bicolor1");
			phonList.item(i).classList.remove("bicolor2");
			if (phonList.item(i).classList.contains('phon_neutre') == false) {
				if (i%2 == 0) {
					phonList.item(i).classList.add("bicolor1");
				} else {
					phonList.item(i).classList.add("bicolor2");
				};
			};
		};
};

function toMonochrome() {
	var phonList = document.getElementsByClassName("phon");
	for (var i = 0; i < phonList.length; i++) {
		phonList.item(i).classList.remove("bicolor1");
		phonList.item(i).classList.remove("bicolor2");
		phonList.item(i).classList.add("monochrome");
	};
}

// Initialisation couleur background (par défaut black)
// if (document.getElementById('defaultBg').value == 'white') {
// 	document.getElementById('outputDiv').classList.toggle('win-white');
// 	document.getElementById('output').classList.toggle('graphContours');
// };
var defaultBg = "black";
var phonA = getComputedStyle(document.documentElement).getPropertyValue('--phon_a')

function bg2white() {
	if (defaultBg == 'black') {
		defaultBg = 'white';
		if (thisPageLang!='zh'){
			document.documentElement.style.setProperty('--phon_a', 'black');
			document.documentElement.style.setProperty('--phon_monochrome', 'black');
		} else {
			document.documentElement.style.setProperty('--sinosvgneutrecolor','black');
			document.documentElement.style.setProperty('--sinosvgborder','solid 1px black');
			document.getElementById('imgzhtokenspace').src = "static/im/zhtokenspacewhite.png";
			//document.documentElement.style.setProperty('--sinosvgtextfill', 'white');
		}

	} else {
		defaultBg = 'black';
		if (thisPageLang!='zh'){
			document.documentElement.style.setProperty('--phon_a', phonA);
			document.documentElement.style.setProperty('--phon_monochrome', 'rgb(218, 218, 218)');
		} else {
			document.documentElement.style.setProperty('--sinosvgneutrecolor','white');
			document.documentElement.style.setProperty('--sinosvgborder','solid 1px white');
			document.getElementById('imgzhtokenspace').src = "static/im/zhtokenspaceblack.png";
			//document.documentElement.style.setProperty('--sinosvgtextfill', 'black');
		}
	}
	document.getElementById('outputDiv').classList.toggle('win-white');
	document.getElementById('output').classList.toggle('graphContours');
	//graph2whiteBg();
}

// Changement des couleurs du bicolor
function graph2whiteBg() {
	var bi1List = document.getElementsByClassName('bicolor1');
	for (var i = 0; i < bi1List.length; i++) {
		//bi1List.item(i).classList.remove("bicolorWhite1");
		//bi1List.item(i).classList.remove("bicolorWhite2");
		bi1List.item(i).classList.add('bicolorWhite1');
	}; 
	var bi2List = document.getElementsByClassName('bicolor2');
	for (var i = 0; i < bi2List.length; i++) {
		//bi1List.item(i).classList.remove("bicolorWhite1");
		//bi1List.item(i).classList.remove("bicolorWhite2");
		bi2List.item(i).classList.add('bicolorWhite2');
	};
}

var displayzi = true;
function togglezi(){
	var zilist = document.getElementsByClassName('sinosvgText');
	if (displayzi){
		for (i=0; i<zilist.length; i++) {
			zilist[i].style.display = "none";
		}
		displayzi = false;
	} else {
		for (i=0; i<zilist.length; i++) {
			zilist[i].style.display = "";
		}
		displayzi = true;
	}
}

var displaywordspace = true;
function togglewordspace(){
	var toklist = document.getElementsByClassName('zhtoken');
	if (displaywordspace){
		document.documentElement.style.setProperty('--zhtokenmargin', '20px 0px');
		document.documentElement.style.setProperty('--sinosvgmargin', '0px 5px');
		displaywordspace = false;
	} else {
		document.documentElement.style.setProperty('--zhtokenmargin', '20px 15px');
		document.documentElement.style.setProperty('--sinosvgmargin', '0px 0px');
		displaywordspace = true;
	}
}

//////////////////////////////////////
////////// PARAMÉTRAGE POPUP /////////
////////// POUR COPIER TEXTE /////////
//////////////////////////////////////
// Get the modal
var modal = document.getElementById("myModal");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// Fonction pour ouvrir le PopUp 
function getPopUp() {
    var outputContent = document.getElementById('output').innerHTML;
    var finalOutput = ""; // contiendra la chaîne de caractères à copier coller
	
	var expFonte = "font-family: Ubuntu, Palatino, SimKai;";
	var expCoulBase = "color:#cccccc;"

	var expSizeBase = "font-size:30pt;";
	var expSizeStress1 = "font-size:45pt;";
	var expSizeStress2 = "font-size:36pt;";
	var expSizeSchwa = "font-size:20pt;";

	var expBold = "font-weight: bold;";
	var expNoBold = "font-weight: normal;";
	var expLong = "text-decoration: underline;";

	// On récupère la liste des <span>
    const regexSpan = /<span class="tokens.*?>(.*?<\/span>)<\/span>|(<span class="phon_neutre">.*?<\/span>)|(<span class="space"> <\/span>)|(<br>)|(<span class="liaison(Obligatoire|Facultative)( phon)? phon_.*?" .*?>.*?<\/span>)/gm;
	// group1: word in colors
	// group2: punctuation
	// group3: space
	// group4: <br>
    // group5: liaison

	let m;
    while ((m = regexSpan.exec(outputContent)) !== null) {
		// parses each result of the regex
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regexSpan.lastIndex) {
            regexSpan.lastIndex++;
        }
        // console.log("group1:",m[1],"group2:",m[2],"group3:",m[3],"group4:",m[4],"group5:",m[5]);
        if (m[5]) {
            var newOutputSpan = "<span style='";
            newOutputSpan += expFonte + expSizeBase + expNoBold;
            
            var phonographiesRegex = /<span class="liaison(Obligatoire|Facultative)( phon)? (.*?)".*?>(.*?)<\/span>/;
			var n = phonographiesRegex.exec(m[5])
            // COULEURS
            var phonClass = "";
            console.log("n[3]:",n[3])
            phonclasses = n[3].split(" "); // phon + phon_*
            for (i=0; i<phonclasses.length; i++) {
                if (phonclasses[i].match(/phon_.*/)) phonClass = phonclasses[i];
                if (phonclasses[i].match(/bicolor\d/)) phonClass = phonclasses[i];
                if (phonclasses[i].match(/monochrome/)) phonClass = "#ffffff";
            }
            if (phonClass != "") newOutputSpan += phon2color(phonClass);

            newOutputSpan += "'>"+n[4]+"</span>";
            finalOutput += newOutputSpan;
        } else if (m[4]) {
            finalOutput += "<br>";
        } else if (m[3]) {
			finalOutput += '<span style="'+expFonte+expCoulBase+expSizeBase+'"> </span>';
		} else if (m[2]) {
			if (boolBold) finalOutput += m[2].replace('class="phon_neutre"','style="'+expFonte+expCoulBase+expSizeBase+expBold+'"');
			else finalOutput += m[2].replace('class="phon_neutre"','style="'+expFonte+expCoulBase+expSizeBase+expNoBold+'"');
		} else if (m[1]) {	
			// Dans cette situation on a une suite de span de phono-graphèmes comme <span class="phon phon_au_maj stress1">hou</span><span class="phon phon_arobase_rho schwa">r</span>
			// On va la transformer en liste de spans pour les traiter une par une
			var phonographiesRegex = /<span class="phon (.*?)">(.*?)<\/span>/gm;
			
			while ((n = phonographiesRegex.exec(m[1])) !== null) {
				// This is necessary to avoid infinite loops with zero-width matches
				if (n.index === phonographiesRegex.lastIndex) {
					phonographiesRegex.lastIndex++;
				}
				
				// The result can be accessed through the `n`-variable.
				//console.log('RESULT:',n[1],n[2]);
				
				var newOutputSpan = "<span style='";
				phonclasses = n[1].split(" ");

				if (phonclasses.includes("unstressed")) newOutputSpan += expFonte + expSizeBase + expNoBold;
				else if (phonclasses.includes("schwa")) newOutputSpan += expFonte + expSizeSchwa + expNoBold;
				else if (phonclasses.includes("stress1")) newOutputSpan += expFonte + expSizeStress1 + expBold;
				else if (phonclasses.includes("stress2")) newOutputSpan += expFonte + expSizeStress2 + expBold;
				else if (phonclasses.includes("phon_echec") || phonclasses.includes("phon_inconnu")) newOutputSpan += expFonte + expCoulBase + expSizeBase + expNoBold;
				else {
					if (boolBold) newOutputSpan += expFonte + expSizeBase + expBold;
					else newOutputSpan += expFonte + expSizeBase + expNoBold;
				}
				
				// COULEURS
				var phonClass = "";
				for (i=0; i<phonclasses.length; i++) {
					if (phonclasses[i].match(/phon_.*/)) phonClass = phonclasses[i];
                    if (phonclasses[i].match(/bicolor\d/)) phonClass = phonclasses[i];
                    if (phonclasses[i].match(/monochrome/)) phonClass = "#ffffff";
				}
				if (phonClass.match(/.*_long/)) newOutputSpan += expLong;
				if (phonClass == "#ffffff") {
                    newOutputSpan += "color:#ffffff;"
                } else if (phonClass != "") newOutputSpan += phon2color(phonClass);

				newOutputSpan += "'>"+n[2]+"</span>";

				finalOutput += newOutputSpan;
			}
			
        }
    }

    // console.log("finalOutput :",finalOutput);
    
	function phon2color(p1){
        var thisRawColor = getRawColor(p1);
        if (thisRawColor == "") {
            // Il s'agit d'un bicolore (actif)
            var col1 = getComputedStyle(document.documentElement).getPropertyValue(bicol2colcol[p1][0]);
			var col2 = getComputedStyle(document.documentElement).getPropertyValue(bicol2colcol[p1][1]);
			var res = 'background-color:'+col1+';color:'+col2+';-webkit-text-stroke-width: 0.5px;-webkit-text-stroke-color: #000000;';
        } else {
            // Il s'agit d'un phonème simple, ou d'un bicolore désactivé (var(--phon_base))
			var col = thisRawColor;
			var res = 'color:'+col+';-webkit-text-stroke-width: 0.5px;-webkit-text-stroke-color: #000000;';
        }
		return res;
    }

    // Remplacement des blancs par des noirs pour le fond blanc
    finalOutputWhite = finalOutput.replace(/#ffffff/g,'#000000');
    finalOutput = finalOutput.replace(/-webkit-text-stroke-width: 0.5px;-webkit-text-stroke-color: #000000;/g,''); // on vire les strokes pour fond noir

	// Insertion des résultats dans les div associées du popup
	document.getElementById('pLienDiv').innerHTML = finalOutput;
    document.getElementById('pLienDivCode').innerHTML = finalOutput;
	document.getElementById('pLienDivWhite').innerHTML = finalOutputWhite;
    document.getElementById('pLienDivWhiteCode').innerHTML = finalOutputWhite;
	// Affichage popup
    modal.style.display = "block";
}



// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
	}
}

// COPIE AUTO DU LIEN DANS PRESSE-PAPIER
function toClipBoard(containerid,tooltipid) {
    //var copyText = document.getElementById("pLienDiv");
    // copyText.select();
    // copyText.setSelectionRange(0, 99999);
	// document.execCommand("copy");

	if (document.selection) {
		var range = document.body.createTextRange();
		range.moveToElementText(document.getElementById(containerid));
		range.select().createTextRange();
		document.execCommand("copy");
	} else if (window.getSelection) {
		var range = document.createRange();
		range.selectNode(document.getElementById(containerid));
		window.getSelection().empty();
		window.getSelection().addRange(range);
		document.execCommand("copy");
	}
	
    
	var tooltip = document.getElementById(tooltipid) ;
	tooltip.innerHTML = "Copié !";
}
    
function outFunc(tooltipid) {
    var tooltip = document.getElementById(tooltipid);
    tooltip.innerHTML = "Copier";
}

// BOUTON POUR AFFICHER LES LIAISONS
function toggleLiaisons() {
    var obl = document.getElementById('liaisonsObligatoires').checked
    var fac = document.getElementById('liaisonsFacultatives').checked

    listObl = Array.from(document.getElementsByClassName('liaisonObligatoire'))
    listFac = Array.from(document.getElementsByClassName('liaisonFacultative'))

    console.log('Modif liaisons : obl=',obl,' fac=',fac)

    if (obl) {
        listObl.forEach( liaisonObl => {
            liaisonObl.innerHTML = '‿'
            liaisonObl.classList.add('phon')
        })
    } else {
        listObl.forEach( liaisonObl => {
            liaisonObl.innerHTML = ' '
            liaisonObl.classList.remove('phon')
        })
    }

    if (fac) {
        listFac.forEach( liaisonFac => {
            liaisonFac.innerHTML = '‿'
            liaisonFac.classList.add('phon')
        })
    } else {
        listFac.forEach( liaisonFac => {
            liaisonFac.innerHTML = ' '
            liaisonFac.classList.remove('phon')
        })
    }

    if (document.getElementById('bicolor').checked == true) {
		toBicolor(); // réinitialisation du bicolor (nécessaire si nb phonèmes différents)
	};
}


/////// Popup export des paramètres
// Get the modal
var modalPar = document.getElementById("popPar");
// Get the <span> element that closes the modal
var spanPar = document.getElementById("closePar");

// Fonction pour ouvrir le PopUp 
function getPopPar() {
    // Affichage popup
    modalPar.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
spanPar.onclick = function() {
    modalPar.style.display = "none";
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modalPar) {
        modalPar.style.display = "none";
    }
}

if (window.location.href.split('!').length>1) importCustom(window.location.href.split('!')[1]);


function setSpaceLength(val){
    document.documentElement.style.setProperty('--space', val/10+'em');
    console.log("Set space length to",val);
}