// let lang2phons = {
//     "fr": {
//         "vowel": ["i","e","ɛ","y","ø","œ","ə","a","u","o","ɔ","ɑ"],
//         "vnasal": ["ɛ̃","œ̃","ɔ̃","ɑ̃"],
//         "semivowel": ["j","ɥ","w"],
//         "nasal": ["m","n","ɲ","ŋ"],
//         "plosive": ["p","b","t","d","k","ɡ"],
//         "fricative": ["f","v","s","z","ʃ","ʒ","ʁ"],
//         "approximant": ["l"],
//         "affricate": ["tʃ","dʒ","ks","ɡz","ts"]
//     },
//     "en": {},
//     "zh": {}
// }

var bi = [
        "a(ɪ)","a(ʊ)","aɪ(ə)","aʊw(ə)","ɑɹ","e(ə)","e(ɪ)","ef","eɹ","ə(ʊ)","ɪ(ə)",
        "(ə)l","(ə)m","(ə)n","(ə)z","(i)z","ɪɹ","(ɪ)z","j(u/ʊ)","j(ə)","j(u)","j(ʊ)","jʊ(ə)",
        "juː","ju","jʊɹ","jʊɹ","o(ʊ)","ɔ(ɪ)","ɔɹ","ʊ(ə)","ʊɹ","wa(ɪ)","waɹ","wɑː","wʌ",
        "wa","wɑ","wɛ̃","ɥi","ij",
        "ɖʐ","ɡɣ","ɡʷ","ɡʒ","ɢʁ","ɟʝ","ɴɢ","ɲ","ɲɟ","ɳɖ","ɳʈ","ŋɡ","ŋʷ","ʈʂ","bβ","ɡb","bv","cç","ɲc","dð","dɮ","dʐ","dʑ","dʒ","dz","ʔh","kʃ","kʷ","ŋk","kp","ks","kw","kx","lj","mɸ","mβ","ŋm","mb","mp","nð","nʃ","nʒ","nθ","nd","nd̪","nj","ns","nt","nt̪","nz","pɸ","pf","ɴq","qχ","sz","tɕ","tɕʰ","tɬ","tʂ","tʂʰ","tʃ","tʃʼ","tθ","ts","tsʰ","tsʼ","xʷ","ɡz"
    ];
var longs = ["ɑː","ɜː","ɔː","iː","uː"]

document.getElementById('colorPicker-phon_base').value = getComputedStyle(document.documentElement).getPropertyValue('--phon_base');

// Mémorisation des couleurs de variables de Phonochromie Alem
let memCol = {};
for (phon in api2class){
    memCol[phon] = getComputedStyle(document.documentElement).getPropertyValue('--'+api2class[phon]).trim();
}

// Identification de la css Phonochromie Alem :
var thisCSS;
for (c=0; c<document.styleSheets.length; c++) {
    if (document.styleSheets[c].href!=null && document.styleSheets[c].href.search(/phonochromie/)>0) { thisCSS = document.styleSheets[c] }
}

// Initialisation memClass, pour enregistrer le contenu d'une classe quand il est désactivé
var memClass = {}

function setPhon(phon) {
    // Modification directe de la css : réactiver /s/ revient à remettre les attributs couleurs de .phon_s enregistrés dans memClass
    // identifier l'entrée de la css à modifier
    for (r=0; r<thisCSS.cssRules.length; r++) {
        if(thisCSS.cssRules[r].selectorText == "."+phon) { 
            // console.log("Modification CSS:",r,thisCSS.cssRules[r])

            // on remet les couleurs enregistrées dans memClass memClass[phon]
            thisCSS.cssRules[r].style['fill'] = memClass[phon]['fill']
            thisCSS.cssRules[r].style['color'] = memClass[phon]['color']
            thisCSS.cssRules[r].style['stop-color'] = memClass[phon]['stop-color']
            thisCSS.cssRules[r].style['-webkit-text-fill-color'] = memClass[phon]['-webkit-text-fill-color']
            thisCSS.cssRules[r].style['text-decoration'] = memClass[phon]['text-decoration']

            if (phon in memClass) delete memClass[phon];
        }
    }
}

function unsetPhon(phon) {
    // Modification directe de la css : désactiver /s/ revient à remplacer les attributs couleurs de .phon_s par .phon_base (et enregistrement des valeurs dans memClass)
    // identifier l'entrée de la css à modifier
    for (r=0; r<thisCSS.cssRules.length; r++) {
        if(thisCSS.cssRules[r].selectorText == "."+phon) { 
            // console.log("Modification CSS:",r,thisCSS.cssRules[r])
            
            // Enregistrement des couleurs dans memClass pour la clé phon
            var [fill,col,stop,webkit,line] = [
                thisCSS.cssRules[r].style['fill'],
                thisCSS.cssRules[r].style['color'], 
                thisCSS.cssRules[r].style['stop-color'],
                thisCSS.cssRules[r].style['-webkit-text-fill-color'],
                thisCSS.cssRules[r].style['text-decoration']
            ]
            memClass[phon] = {'fill':fill, 'color':col, 'stop-color':stop, '-webkit-text-fill-color':webkit, 'text-decoration':line}

            // Suppression indications couleurs dans la css
            thisCSS.cssRules[r].style['fill'] = 'var(--phon_base)'
            thisCSS.cssRules[r].style['color'] = 'var(--phon_base)'
            thisCSS.cssRules[r].style['stop-color'] = 'var(--phon_base)'
            thisCSS.cssRules[r].style['-webkit-text-fill-color'] = ''
            // if (thisCSS.cssRules[r].style['text-decoration'] != '') thisCSS.cssRules[r].style['text-decoration'] = 'underline var(--phon_base)'
            if (thisCSS.cssRules[r].style['text-decoration'] != '') thisCSS.cssRules[r].style['text-decoration'] = ''
        }
    }
    
}

function getRawColor(phon) {
    for (r=0; r<thisCSS.cssRules.length; r++) {
        if(thisCSS.cssRules[r].selectorText == "."+phon) { 
            var thisCssVar = thisCSS.cssRules[r].style["color"];
            // console.log(thisCssVar.substring(4,thisCssVar.length-1));
            return getComputedStyle(document.documentElement).getPropertyValue(thisCssVar.substring(4,thisCssVar.length-1));
        }
    }
}
//------------------------------------------------
//>>>>>>>>>>>>> document.styleSheets[3].cssRules[69].style.fill
// -------------------------------------------------

function setCol(phon, color) {
    document.documentElement.style.setProperty('--'+phon, color);
    console.log("Set color of",phon,"to",color);
}

function getCol(phon) {
    return getComputedStyle(document.documentElement).getPropertyValue('--'+phon);
}

function togglePhon(cbid) {
    var thisCb = document.querySelector('#'+cbid);
    var phon = cbid.slice(3);
    if (thisCb) {
        console.log(thisCb.checked, phon);
        if (thisCb.checked) {
            setPhon(phon)
        } else {
            unsetPhon(phon);
        }
    } else {
        console.log('deactivate',phon);
        unsetPhon(phon);
    }    
}

function checkUncheck(thisChecked, classe) {
    cblist = document.querySelectorAll('.'+classe);
    console.log(thisChecked);
    if (!thisChecked) {
        for (i=0; i<cblist.length; i++) {
            if (cblist[i].checked == true){
                cblist[i].checked = false;
                togglePhon(cblist[i].id)
            }
        }
    } else {
        for (i=0; i<cblist.length; i++) {
            if (cblist[i].checked == false){
                cblist[i].checked = true;
                togglePhon(cblist[i].id)
            }
        }
    }
}

function selectRegion(region) {
    thisRegion = region
    makePhonTable(thisPageLang)
}

function makePhonTable(lang) {
    if (lang == 'fr') { 
        document.getElementById('phonTable').innerHTML = phonTableFr;
    } else if (lang == 'en') { 
        if (thisRegion == 'UK') document.getElementById('phonTable').innerHTML = phonTableEnBr;
        else if (thisRegion == 'US') document.getElementById('phonTable').innerHTML = phonTableEnUS;
        else document.getElementById('phonTable').innerHTML = phonTableEnUS;
        
    } else if (lang == 'zh') { 
        document.getElementById('phonTable').innerHTML = phonTableZh;
    }

    // GENERATION DES CHECKBOX+COLORPICKER
    custphonList = document.querySelectorAll('.custphonc');
    for (i=0; i<custphonList.length; i++) {
        var api = custphonList[i].dataset.phon;
        var colpick = ' <input type="color" id="colorPicker-'+api2class[api]+'" class="colorSetting" onchange="setCol(\''+api2class[api]+'\',this.value)" value="'+ getCol(api2class[api]).trim() +'">';
        if (bi.includes(api)){
            colpick = '';
        }
        custphonList[i].innerHTML = '<div class="form-check m-1"><input class="form-check-input check-phon check-cons" type="checkbox" onchange="togglePhon(this.id)" id="cb-'+api2class[api]+'" checked><label class="form-check-label" for="cb-'+api2class[api]+'">'+api+colpick+'</label></div>';
    }

    custphonList = document.querySelectorAll('.custphonv');
    for (i=0; i<custphonList.length; i++) {
        var api = custphonList[i].dataset.phon;
        var colpick = ' <input type="color" id="colorPicker-'+api2class[api]+'" class="colorSetting" onchange="setCol(\''+api2class[api]+'\',this.value)" value="'+ getCol(api2class[api]).trim() +'">';
        if (bi.includes(api)){
            colpick = '';
        }
        custphonList[i].innerHTML = '<div class="form-check m-1"><input class="form-check-input check-phon check-vow" type="checkbox" onchange="togglePhon(this.id)" id="cb-'+api2class[api]+'" checked><label class="form-check-label" for="cb-'+api2class[api]+'">'+api+colpick+'</label></div>';
    }

    custphonList = document.querySelectorAll('.custphonvn');
    for (i=0; i<custphonList.length; i++) {
        var api = custphonList[i].dataset.phon;
        var colpick = ' <input type="color" id="colorPicker-'+api2class[api]+'" class="colorSetting" onchange="setCol(\''+api2class[api]+'\',this.value)" value="'+ getCol(api2class[api]).trim() +'">';
        if (bi.includes(api)){
            colpick = '';
        }
        custphonList[i].innerHTML = '<div class="form-check m-1"><input class="form-check-input check-phon check-vownas" type="checkbox" onchange="togglePhon(this.id)" id="cb-'+api2class[api]+'" checked><label class="form-check-label" for="cb-'+api2class[api]+'">'+api+colpick+'</label></div>';
    }

    custphonList = document.querySelectorAll('.custphonschwa');
    for (i=0; i<custphonList.length; i++) {
        var api = custphonList[i].dataset.phon;
        var colpick = ' <input type="color" id="colorPicker-'+api2class[api]+'" class="colorSetting" onchange="setCol(\''+api2class[api]+'\',this.value)" value="'+ getCol(api2class[api]).trim() +'">';
        if (bi.includes(api)){
            colpick = '';
        }
        custphonList[i].innerHTML = '<div class="form-check m-1"><input class="form-check-input check-phon check-schwa" type="checkbox" onchange="togglePhon(this.id)" id="cb-'+api2class[api]+'" checked><label class="form-check-label" for="cb-'+api2class[api]+'">'+api+colpick+'</label></div>';
    }

    custphonList = document.querySelectorAll('.custphonvowOther');
    for (i=0; i<custphonList.length; i++) {
        var api = custphonList[i].dataset.phon;
        var colpick = ' <input type="color" id="colorPicker-'+api2class[api]+'" class="colorSetting" onchange="setCol(\''+api2class[api]+'\',this.value)" value="'+ getCol(api2class[api]).trim() +'">';
        if (bi.includes(api)){
            colpick = '';
        }
        custphonList[i].innerHTML = '<div class="form-check m-1"><input class="form-check-input check-phon check-vowOther" type="checkbox" onchange="togglePhon(this.id)" id="cb-'+api2class[api]+'" checked><label class="form-check-label" for="cb-'+api2class[api]+'">'+api+colpick+'</label></div>';
    }

    // déchecker tous les phonèmes déjà désactivés (listés dans memClass)
    for (phon in memClass) {
        var thisCb = document.getElementById('cb-'+phon);
        if (thisCb != null) {
            document.getElementById('cb-'+phon).checked = false;
        }
    }
}


function exportCustom(){
    var paramlink = "";

    // Récupération liste des phonèmes inactifs
    var unchecks = "";
    for (i in memClass){
        unchecks += i.replace('phon_','')+'&';
    }
    unchecks = unchecks.replace(/&$/,'');

    // Récupération liste des couleurs customisées
    var cols = document.documentElement.style.cssText.replace(/--space:.*?em;/,'')
                                                        .replace(/ /g,'')
                                                        .replace(/--phon_/g,'')
                                                        .replace(/#/g,'')
                                                        .replace(/:/g,'=')
                                                        .replace(/;/g,'&')
                                                        .replace(/&$/,'')

    // Mémorisation si bicolore ou pas
    var bicol = document.getElementById('bicolor').checked;

    // Mémorisation si monochrome ou pas
    var mnc = document.getElementById('monochrome').checked;

    // Mémorisation liaisons
    var obl = 1;
    if(!document.getElementById('liaisonsObligatoires').checked) obl=0;
    var fac = 1;
    if(!document.getElementById('liaisonsFacultatives').checked) fac=0;

    // Mémorisation taille des espaces
    var spaceLen = getComputedStyle(document.documentElement).getPropertyValue('--space');

    // Génération du lien
    var params = [];
    if(unchecks.length>0) params.push('uck'+unchecks);
    if(cols.length>0) params.push('col'+cols);
    if(bicol) params.push('bic');
    if(mnc) params.push('mnc');
    if(obl==0 || fac==0) params.push('lia'+String(obl)+String(fac));
    if(spaceLen!="normal") {
        spaceLen = spaceLen.replace(/em/,'')*10;
        params.push('spa'+spaceLen);
    }

    if(defaultBg == "white") params.push('bgw');
    if(thisRegion!="FR") params.push('reg'+thisRegion);

    paramlink = window.location.href + "/!" + params.join('$');
    console.log(paramlink);
    document.getElementById('pLienDivPar').innerHTML = paramlink;
    getPopPar();
}

function importCustom(paramlink){
    
    var par = paramlink.split('$');
    var p2param = {};
    for (let x of par) {
        var p = x.slice(0,3);
        var param = x.slice(3,x.length).split('&');
        p2param[p] = param;
    }
    console.log("Import des paramètres :",p2param);

    if ("uck" in p2param) {
        for (let phon of p2param['uck']) {
            var thisCb = document.querySelector('#cb-phon_'+phon);
            if (thisCb) thisCb.checked = false;
            togglePhon('cb-phon_'+phon);
        }
    }

    if ("col" in p2param) {
        for (let col of p2param['col']) {
            var [phon,hex] = col.split('=');
            var thisColPick = document.querySelector('#colorPicker-phon_'+phon);
            if(thisColPick) {
                thisColPick.value = "#"+hex;
            }
            setCol('phon_'+phon,'#'+hex);
        }
    }

    if ("bic" in p2param) {
        console.log('toBicolor()!');
        toBicolor();
        document.getElementById('bicolor').checked = true;
    }

    if ("mnc" in p2param) {
        console.log('toMonochrome()!');
        toMonochrome();
        document.getElementById('monochrome').checked = true;
    }

    if ("reg" in p2param) {
        console.log('Region set to',p2param['reg']);
        thisRegion = p2param['reg'];
        document.getElementById('inlineRadio'+p2param['reg']).checked = true;
    }

    if ("bgw" in p2param) {
        bg2white();
    }

    if ("lia" in p2param) {
        if (p2param['lia'][0]=="01" || p2param['lia'][0]=="00"){
            console.log('lia obl set to false');
            document.getElementById('liaisonsObligatoires').checked = false;
        }
        if (p2param['lia']=="10" || p2param['lia']=="00"){
            console.log('lia fac set to false');
            document.getElementById('liaisonsFacultatives').checked = false;
        }
        toggleLiaisons();
    }

    if ("spa" in p2param) {
        document.getElementById('spaceLenInput').value = p2param['spa'][0];
        setSpaceLength(p2param['spa'][0]);
    }
}
