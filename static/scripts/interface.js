const thisURL = window.location.href;
console.log("url:",thisURL);
if (typeof thisRegion == 'undefined') var thisRegion = '';

var boolBold = true;
function toggleBold() {
	if (document.getElementById('output')) {document.getElementById('output').classList.toggle('outputBold');}
	boolBold = !boolBold;
	console.log("boolBold to",boolBold);
}

// set page target language
setLangFromUrl()
function setLangFromUrl() {
    var pageLang = thisURL.match(/.*\/(fr|en|zh)\/?.*/);
    if (pageLang) { 
        console.log("Langue indiquée par l'url:",pageLang[1]);
        thisPageLang = pageLang[1];
        if (pageLang[1]=="fr") selectLang("fr");
		if (pageLang[1]=="en") selectLang("en");
		if (pageLang[1]=="zh") selectLang("zh");
    } else { 
        console.log("Chargement langue par défaut (fr)");
        thisPageLang = "fr"
        selectLang("fr");
    }
}

function selectLang(lang){
	if (lang=="en"){
		interface("en");
		document.getElementById('choixLang').value = 'en';
		if (boolBold) toggleBold();
		
        document.getElementById('zhsubtitle').style.display="none";
        document.getElementById('subtitle').style.marginTop = "-20px";
        
        if(document.getElementById('silentWayLabel')){ // if this page is Wikicolor's main page
            document.getElementById('monochromeLabel').style.display="";
            document.getElementById('monochrome').style.display="";
            document.getElementById('silentWayLabel').style.margin = "-20px 0px";
            document.getElementById('bicolor').style.display="";
            document.getElementById('bicolorLabel').style.display="";
            document.getElementById('bicolorLabel').style.margin = "-10px 0px";

            // Paramètres output
            document.getElementById('ti_btnCopierColler').style.display = "";
            document.getElementById('ti_btnBold').style.display = "";
            document.getElementById('ti_btnzi').style.display = "none";
            document.getElementById('ti_btnwordspace').style.display = "none";
            document.getElementById('btnLiaison').style.display = "none";
            window.history.pushState("", "", "/en");

            // Region settings
            document.getElementById('regionPriorityDiv').style.display = '';
            thisRegion = "UK";

            // Custom Settings
            document.getElementById('customSettings').style.display = '';
        }


	} else if (lang=="fr"){
		interface("fr");
		document.getElementById('choixLang').value = 'fr';
		if (boolBold == false) toggleBold();

        document.getElementById('zhsubtitle').style.display="none";
        document.getElementById('subtitle').style.marginTop = "0px";

        if(document.getElementById('silentWayLabel')){ // if this page is Wikicolor's main page
            document.getElementById('monochromeLabel').style.display="none";
            document.getElementById('monochrome').style.display="none";
            document.getElementById('silentWayLabel').style.margin = "0px 0px";
            document.getElementById('bicolor').style.display="";
            document.getElementById('bicolorLabel').style.display="";
            document.getElementById('bicolorLabel').style.margin = "0px 0px";

            // Paramètres output
            document.getElementById('ti_btnCopierColler').style.display = "";
            document.getElementById('ti_btnBold').style.display = "";
            document.getElementById('ti_btnzi').style.display = "none";
            document.getElementById('ti_btnwordspace').style.display = "none";
            document.getElementById('btnLiaison').style.display = "";
            window.history.pushState("", "", "/fr");
            
            // Region settings
            document.getElementById('regionPriorityDiv').style.display = 'none';
            thisRegion = "FR";

            // Custom Settings
            document.getElementById('customSettings').style.display = '';
        }

	} else if (lang == "zh"){
		interface("zh");
		document.getElementById('choixLang').value = 'zh';
		if (boolBold == false) toggleBold();

        document.getElementById('zhsubtitle').style.display="";
        document.getElementById('subtitle').style.marginTop = "0px";

        if(document.getElementById('silentWayLabel')){ // if this page is Wikicolor's main page
            document.getElementById('monochromeLabel').style.display="none";
            document.getElementById('monochrome').style.display="none";
            document.getElementById('silentWayLabel').style.margin = "0px 0px";
            document.getElementById('bicolor').style.display="none";
            document.getElementById('bicolorLabel').style.display="none";

            // Paramètres output
            document.getElementById('ti_btnCopierColler').style.display = "none";
            document.getElementById('ti_btnBold').style.display = "none";
            document.getElementById('ti_btnzi').style.display = "";
            document.getElementById('ti_btnwordspace').style.display = "";
            document.getElementById('btnLiaison').style.display = "none";
            window.history.pushState("", "", "/zh");

            // Region settings
            document.getElementById('regionPriorityDiv').style.display = 'none';
            thisRegion = "CN";

            // Custom Settings
            document.getElementById('customSettings').style.display = 'none';
        }

	}
    // Phon Table
    // if this function is defined (wikicolor main page)
    if (typeof(makePhonTable)==='function') { makePhonTable(lang) };
}

function interface(lang) {
    console.log("Langue d'interface:",lang);
    thisPageLang = lang;
	var langspanList = document.getElementsByClassName("langspan");
	var langtitleList = document.getElementsByClassName("langtitle");
    if (lang == "en") {
        for (i=0; i<langspanList.length; i++) {
            span = langspanList[i];
            span.innerHTML = langJson[span.id]["en"];
		}
		for (i=0; i<langtitleList.length; i++) {
			title = langtitleList[i];
			title.title = langJson[title.id]["en"];
		}

	} else if (lang == "zh") {
		for (i=0; i<langspanList.length; i++) {
            span = langspanList[i];
            span.innerHTML = langJson[span.id]["zh"];
		}
		for (i=0; i<langtitleList.length; i++) {
			title = langtitleList[i];
			title.title = langJson[title.id]["zh"];
		}
	
    } else { // "fr" par défaut
        for (i=0; i<langspanList.length; i++) {
            span = langspanList[i];
            span.innerHTML = langJson[span.id]["fr"];
		}
		for (i=0; i<langtitleList.length; i++) {
			title = langtitleList[i];
			title.title = langJson[title.id]["fr"];
		}
    }
}

async function addStat(modul) {
	var lang = document.getElementById('choixLang').value;

	// ON EMBALLE TOUT ÇA
    var colis = {
		app: "wikicolor",
        module: modul,
		lang: lang
	}

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(colis)
    };
    
    // ENVOI
	await fetch('/addStat/', options);
}