// create('三',3,["phon_s","phon_a","phon_n"],1)
// create('六',3,["phon_l","phon_j","phon_o"],4)
// create('一',1,["phon_i"],1)
// create('四',2,["phon_s","phon_1"],4)
// create('二',2,["phon_a","phon_neutre"],4)
// create('五',1,["phon_u"],3)
// create('九',3,["phon_ts_slash","phon_j","phon_o"],3)
// create('八',2,["phon_p","phon_a"],1)
// create('十',2,["phon_s_retr","phon_1"],2)
// create('两',4,["phon_l","phon_j","phon_a","phon_n_maj"],3)
// create('七',2,["phon_t_hs_slash","phon_i"],1)
// create('万',3,["phon_w","phon_a","phon_n"],4)
// create('千',4,["phon_t_hs_slash","phon_j","phon_e_maj","phon_n"],1)
// create('零',3,["phon_l","phon_i","phon_n_maj"],2)
// create('百',3,["phon_p","phon_a","phon_j"],3)
// create('我',2,["phon_w","phon_o"],3)
// create('要',3,["phon_j","phon_a","phon_w"],4)
// create('去',2,["phon_t_hs_slash","phon_y"],4)
// create('中',3,["phon_ts_retr","phon_o","phon_n_maj"],1)
// create('国',3,["phon_k","phon_w","phon_o"],2)
// create('中',3,["phon_ts_retr","phon_o","phon_n_maj"],"all")


function createZh(sin,sy,phonList,ton) {

    // create the svg element
    const svg1 = document.createElementNS("http://www.w3.org/2000/svg", "svg");

    // set width and height
    svg1.setAttribute("width", "100");
    svg1.setAttribute("height", "100");

    svg1.setAttribute("class","sinosvg");

    // 
    if (sy == 1) {
        const rect1 = document. createElementNS("http://www.w3.org/2000/svg", "rect");
        rect1.setAttribute("x", "0");
        rect1.setAttribute("y", "0");
        rect1.setAttribute("height", "100");
        rect1.setAttribute("width", "100");
        rect1.setAttribute("stroke","black");
        rect1.setAttribute("stroke-width","0.5");
        rect1.setAttribute("class", phonList[0]);

        // attach it to the container
        svg1.appendChild(rect1);

    } else if (sy == 2) {
        const rect1 = document. createElementNS("http://www.w3.org/2000/svg", "rect");
        rect1.setAttribute("x", "0");
        rect1.setAttribute("y", "0");
        rect1.setAttribute("height", "50");
        rect1.setAttribute("width", "100");
        rect1.setAttribute("stroke","black");
        rect1.setAttribute("stroke-width","0.5");
        rect1.setAttribute("class", phonList[0]);
        const rect2 = document. createElementNS("http://www.w3.org/2000/svg", "rect");
        rect2.setAttribute("x", "0");
        rect2.setAttribute("y", "50");
        rect2.setAttribute("height", "50");
        rect2.setAttribute("width", "100");
        rect2.setAttribute("stroke","black");
        rect2.setAttribute("stroke-width","0.5");
        rect2.setAttribute("class", phonList[1]);

        // attach it to the container
        svg1.appendChild(rect1);
        svg1.appendChild(rect2);

    } else if (sy == 3) {
        const rect1 = document. createElementNS("http://www.w3.org/2000/svg", "rect");
        rect1.setAttribute("x", "0");
        rect1.setAttribute("y", "0");
        rect1.setAttribute("height", "33");
        rect1.setAttribute("width", "100");
        rect1.setAttribute("stroke","black");
        rect1.setAttribute("stroke-width","0.5");
        rect1.setAttribute("class", phonList[0]);
        const rect2 = document. createElementNS("http://www.w3.org/2000/svg", "rect");
        rect2.setAttribute("x", "0");
        rect2.setAttribute("y", "33");
        rect2.setAttribute("height", "33");
        rect2.setAttribute("width", "100");
        rect2.setAttribute("stroke","black");
        rect2.setAttribute("stroke-width","0.5");
        rect2.setAttribute("class", phonList[1]);
        const rect3 = document. createElementNS("http://www.w3.org/2000/svg", "rect");
        rect3.setAttribute("x", "0");
        rect3.setAttribute("y", "66");
        rect3.setAttribute("height", "33");
        rect3.setAttribute("width", "100");
        rect3.setAttribute("stroke","black");
        rect3.setAttribute("stroke-width","0.5");
        rect3.setAttribute("class", phonList[2]);


        // attach it to the container
        svg1.appendChild(rect1);
        svg1.appendChild(rect2);
        svg1.appendChild(rect3);

    } else if (sy == 4) {
        const rect1 = document. createElementNS("http://www.w3.org/2000/svg", "rect");
        rect1.setAttribute("x", "0");
        rect1.setAttribute("y", "0");
        rect1.setAttribute("height", "25");
        rect1.setAttribute("width", "100");
        rect1.setAttribute("stroke","black");
        rect1.setAttribute("stroke-width","0.5");
        rect1.setAttribute("class", phonList[0]);
        const rect2 = document. createElementNS("http://www.w3.org/2000/svg", "rect");
        rect2.setAttribute("x", "0");
        rect2.setAttribute("y", "25");
        rect2.setAttribute("height", "25");
        rect2.setAttribute("width", "100");
        rect2.setAttribute("stroke","black");
        rect2.setAttribute("stroke-width","0.5");
        rect2.setAttribute("class", phonList[1]);
        const rect3 = document. createElementNS("http://www.w3.org/2000/svg", "rect");
        rect3.setAttribute("x", "0");
        rect3.setAttribute("y", "50");
        rect3.setAttribute("height", "25");
        rect3.setAttribute("width", "100");
        rect3.setAttribute("stroke","black");
        rect3.setAttribute("stroke-width","0.5");
        rect3.setAttribute("class", phonList[2]);
        const rect4 = document. createElementNS("http://www.w3.org/2000/svg", "rect");
        rect4.setAttribute("x", "0");
        rect4.setAttribute("y", "75");
        rect4.setAttribute("height", "25");
        rect4.setAttribute("width", "100");
        rect4.setAttribute("stroke","black");
        rect4.setAttribute("stroke-width","0.5");
        rect4.setAttribute("class", phonList[3]);

        
        // attach it to the container
        svg1.appendChild(rect1);
        svg1.appendChild(rect2);
        svg1.appendChild(rect3);
        svg1.appendChild(rect4);
        
    
    }

    // TEXTE
    const txt = document.createElementNS("http://www.w3.org/2000/svg", "text");
    txt.setAttribute("x","0");
    txt.setAttribute("y","85");
    //txt.setAttribute("font-family", 'KaiTi' ); // Ma Shan Zheng 'AR PL UKai TW'
    txt.setAttribute("font-size","100");
    txt.setAttribute("class","sinosvgText");
    txt.innerHTML = sin;
    svg1.appendChild(txt);

    // TON
    // r1 r7 r4
    // r2    r5
    // r3 r8 r6
    const tailleCarre = 12;
    const fillCarre = "black";

    const r1 = document. createElementNS("http://www.w3.org/2000/svg", "rect");
    r1.setAttribute("x", "0");
    r1.setAttribute("y", "0");
    r1.setAttribute("height", tailleCarre);
    r1.setAttribute("width", tailleCarre);
    r1.setAttribute("fill",fillCarre);
    const r2 = document. createElementNS("http://www.w3.org/2000/svg", "rect");
    r2.setAttribute("x", "0");
    r2.setAttribute("y", 50-tailleCarre/2);
    r2.setAttribute("height", tailleCarre);
    r2.setAttribute("width", tailleCarre);
    r1.setAttribute("fill",fillCarre);
    const r3 = document. createElementNS("http://www.w3.org/2000/svg", "rect");
    r3.setAttribute("x", "0");
    r3.setAttribute("y", 100-tailleCarre);
    r3.setAttribute("height", tailleCarre);
    r3.setAttribute("width", tailleCarre);
    r3.setAttribute("fill",fillCarre);
    const r4 = document. createElementNS("http://www.w3.org/2000/svg", "rect");
    r4.setAttribute("x", 100-tailleCarre);
    r4.setAttribute("y", 0);
    r4.setAttribute("height", tailleCarre);
    r4.setAttribute("width", tailleCarre);
    r4.setAttribute("fill",fillCarre);
    const r5 = document. createElementNS("http://www.w3.org/2000/svg", "rect");
    r5.setAttribute("x", 100-tailleCarre);
    r5.setAttribute("y", 50-tailleCarre/2);
    r5.setAttribute("height", tailleCarre);
    r5.setAttribute("width", tailleCarre);
    r5.setAttribute("fill",fillCarre);
    const r6 = document. createElementNS("http://www.w3.org/2000/svg", "rect");
    r6.setAttribute("x", 100-tailleCarre);
    r6.setAttribute("y", 100-tailleCarre);
    r6.setAttribute("height", tailleCarre);
    r6.setAttribute("width", tailleCarre);
    r6.setAttribute("fill",fillCarre);
    const r7 = document. createElementNS("http://www.w3.org/2000/svg", "rect");
    r7.setAttribute("x", 50-tailleCarre/2);
    r7.setAttribute("y", 0);
    r7.setAttribute("height", tailleCarre);
    r7.setAttribute("width", tailleCarre);
    r7.setAttribute("fill",fillCarre);
    const r8 = document. createElementNS("http://www.w3.org/2000/svg", "rect");
    r8.setAttribute("x", 50-tailleCarre/2);
    r8.setAttribute("y", 100-tailleCarre);
    r8.setAttribute("height", tailleCarre);
    r8.setAttribute("width", tailleCarre);
    r8.setAttribute("fill",fillCarre);


    if (ton == "all") {
        svg1.appendChild(r1);
        svg1.appendChild(r2);
        svg1.appendChild(r3);
        svg1.appendChild(r4);
        svg1.appendChild(r5);
        svg1.appendChild(r6);
        svg1.appendChild(r7);
        svg1.appendChild(r8);

    } else if (ton == "1") {
        svg1.appendChild(r1);
        svg1.appendChild(r4);

    } else if (ton == "2") {
        svg1.appendChild(r2);
        svg1.appendChild(r4);
        
    } else if (ton == "3") {
        svg1.appendChild(r2);
        svg1.appendChild(r8);
        svg1.appendChild(r4);
        
    } else if (ton == "4") {
        svg1.appendChild(r1);
        svg1.appendChild(r6);
    }


    // attach container to document
    return svg1;
}