// KEYBOARD
// inspiré de https://mottie.github.io/Keyboard/
$.keyboard.keyaction.undo = function (base) {
	base.execCommand('undo');
	return false;
};
$.keyboard.keyaction.redo = function (base) {
	base.execCommand('redo');
	return false;
};


var lang2keyboard = {
    "fr": [
        'ɛ ø œ ə ɔ ɑ ɥ \u0303',
        'ʃ ʒ ɡ ʁ ɲ ŋ'
    ],
    "en": [
        'ɪ ɛ æ ʊ ə ɚ ʌ ʉ ɜ ɝ ɒ ɔ ɑ ː ˈ ˌ',
        'θ ð ʃ ʒ ɡ ɹ ŋ'
    ],
    "zh": [
        ''
    ],
    "all": [
        'ɛ ø œ ə ɔ ɑ ɥ \u0303 ʃ ʒ ɡ ʁ ɲ ŋ',
        'ɪ ɛ æ ʊ ə ɚ ʌ ʉ ɜ ɝ ɒ ɔ ɑ ː θ ð ʃ ʒ ɡ ɹ ŋ'
    ]
}

if (typeof dicoLang !== 'undefined') {
    getAPIkeyboard(dicoLang);
} else {
    getAPIkeyboard(thisPageLang);
}
function getAPIkeyboard(lang) {
	var apizones = Object.entries($('.api-kb'));

	for (i = 0 ; i < apizones.length-2 ; i++) {
	$(apizones[i][1]).keyboard({
			usePreview: false, // disabled for contenteditable
			useCombos: false,
			autoAccept: true,
			layout: 'custom',
			customLayout: {
				'normal': lang2keyboard[lang],
			},
			display: {
				del: '\u2326:Delete',
				redo: '↻',
				undo: '↺'
			}
		});
	};
}
