let api2class = {
    "ɨ":"phon_1",
	"ø":"phon_2",
	"ɜ":"phon_3",
	"ɞ":"phon_3_slash",
	"ɜː":"phon_3_long",
	"ɝ":"phon_3_rho",
	"ɾ":"phon_4",
    "ɫ":"phon_5",
    "lˠ":"phon_5",
	"ɐ":"phon_6",
	"ɤ":"phon_7",
	"ɵ":"phon_8",
	"œ":"phon_9",
	"ɶ":"phon_9_maj",
	"œ̃":"phon_9_nas",
	"a":"phon_a",
	"ä":"phon_a_centr",
	"ɑ":"phon_a_maj",
	"ɑː":"phon_a_maj_long",
	"ɑ̃":"phon_a_maj_nas",
	"ə":"phon_arobase",
	"ɚ":"phon_schwarho",
	"a˞":"phon_arho",
	"ɤ˞":"phon_7rho",
	"ɘ":"phon_arobase_slash",
	"b":"phon_b",
	"ɓ":"phon_b_chev",
	"β":"phon_b_maj",
	"ʙ":"phon_b_maj_slash",
	"c":"phon_c",
	"ç":"phon_c_maj",
	"æ":"phon_cbrack",
	"ʉ":"phon_cbrack2",
	"ʢ":"phon_chevron_slash",
	"ʡ":"phon_chevron2_slash",
	"d":"phon_d",
	"ɗ":"phon_d_chev",
	"ð":"phon_d_maj",
	"ɖ":"phon_d_retr",
	"e":"phon_e",
	"ɛ":"phon_e_maj",
	"ɛ̃":"phon_e_maj_nas",
	"ǂ":"phon_egal_slash",
	"ǃ":"phon_exclam_slash",
	"f":"phon_f",
	"ɱ":"phon_f_maj",
	"ɡ":"phon_g",
	"ɠ":"phon_g_chev",
	"ɣ":"phon_g_maj",
	"ɢ":"phon_g_maj_slash",
	"ʛ":"phon_g_maj_slash_chev",
	"ʔ":"phon_glottstop",
	"ʕ":"phon_glottstop_slash",
	"h":"phon_h",
	"ɥ":"phon_h_maj",
	"ʜ":"phon_h_maj_slash",
	"ɦ":"phon_h_slash",
	"i":"phon_i",
	"iː":"phon_i_long",
	"ɪ":"phon_i_maj",
	"j":"phon_j",
	"ʝ":"phon_j_slash",
	"ɟ":"phon_j_maj_slash",
	"ʄ":"phon_j_maj_slash_chev",
	"k":"phon_k",
	"kʼ":"phon_k_chev2",
	"kʰ":"phon_k_h",
	"ɬ":"phon_k_maj",
	"ɮ":"phon_k_maj_slash",
	"l":"phon_l",
	"ʎ":"phon_l_maj",
	"ʟ":"phon_l_maj_slash",
	"ɭ":"phon_l_retr",
	"m":"phon_m",
	"ɯ":"phon_m_maj",
	"ɰ":"phon_m_maj_slash",
	"n":"phon_n",
	"ŋ":"phon_n_maj",
	"ɴ":"phon_n_maj_slash",
	"ɳ":"phon_n_retr",
	"o":"phon_o",
	"ɔ":"phon_o_maj",
	"ɔː":"phon_o_maj_long",
	"ɔ̃":"phon_o_maj_nas",
	"ʘ":"phon_o_maj_slash",
	"p":"phon_p",
	"pʼ":"phon_p_chev2",
	"p̪":"phon_p_d",
	"pʰ":"phon_p_h",
	"ʋ":"phon_p_maj",
	"ɸ":"phon_p_slash",
	"ǀ":"phon_pipe_slash",
	"ǁ":"phon_pipe_slashpipe_slash",
	"q":"phon_q",
	"qʼ":"phon_q_chev2",
	"ɒ":"phon_q_maj",
	"r":"phon_r",
	"r̥":"phon_r_0",
	"ʁ":"phon_r_maj",
	"ʀ":"phon_r_maj_slash",
	"ɽ":"phon_r_retr",
	"ɹ":"phon_r_slash",
	"ɻ":"phon_r_slash_retr",
	"s":"phon_s",
	"sʼ":"phon_s_chev2",
	"ʃ":"phon_s_maj",
	"ʃʼ":"phon_s_maj_chev2",
	"ʂ":"phon_s_retr",
	"ɕ":"phon_s_slash",
	"t":"phon_t",
	"tʼ":"phon_t_chev2",
	"tʰ":"phon_t_h",
	"θ":"phon_t_maj",
	"ʈ":"phon_t_retr",
	"ʈʼ":"phon_t_retr_chev2",
	"u":"phon_u",
	"uː":"phon_u_long",
	"ʊ":"phon_u_maj",
	"v":"phon_v",
	"ⱱ":"phon_v_flap",
	"ʌ":"phon_v_maj",
	"w":"phon_w",
	"ʍ":"phon_w_maj",
	"x":"phon_x",
	"xʼ":"phon_x_chev2",
	"χ":"phon_x_maj",
	"ħ":"phon_x_maj_slash",
	"ɧ":"phon_x_slash",
	"y":"phon_y",
	"ʏ":"phon_y_maj",
	"z":"phon_z",
	"ʒ":"phon_z_maj",
	"ʐ":"phon_z_retr",
	"ʑ":"phon_z_slash",

	"(i/ɪ)":"phon_schwi",
	"(ə)":"phon_schwa",
    "(ɚ)":"phon_schwarho",
	"(u/ʊ)":"phon_schwu",
	"j(u/ʊ)":"phon_schwju",
	"(ɪ)z":"phon_schwiz",
	"(ə)z":"phon_schwaz",
	"ɑɹ":"phon_a_majr",
	"a(ɪ)":"phon_aschwi",
	"aɪ(ə)":"phon_ai_majschwa",
	"a(ʊ)":"phon_aschwu",
	"aʊw(ə)":"phon_au_majwschwa",
	"(ə)l":"phon_schwal",
	"(ə)m":"phon_schwam",
	"(ə)n":"phon_schwan",
	"ə(ʊ)":"phon_arobaseschwu",
	"ef":"phon_ef",
	"e(ɪ)":"phon_eschwi",
	"e(ə)":"phon_eschwa",
	"eɹ":"phon_er",
	"ɪ(ə)":"phon_i_majschwa",
	"jʊ(ə)":"phon_ju_majschwa",
	"ɥi":"phon_h_maj_i",
	"ij":"phon_ij",
	"ɪɹ":"phon_i_majr",
	"j(ə)":"phon_jschwa",
	"juː":"phon_ju_long",
    "ju":"phon_ju",
	"jʊɹ":"phon_ju_majr",
	"o(ʊ)":"phon_oschwu",
	"ɔ(ɪ)":"phon_o_majschwi",
	"ɔɹ":"phon_o_majr",
	"ʊ(ə)":"phon_u_majschwa",
	"ʊɹ":"phon_u_majr",
	"wa":"phon_wa",
	"waɹ":"phon_war",
	"wɑ":"phon_wa_maj",
	"wɑː":"phon_wa_maj_long",
	"wa(ɪ)":"phon_waschwi",
	"wɛ̃":"phon_we_maj_nas",
	"wʌ":"phon_wv_maj",
    "jɝ":"phon_j3_rho",
    "(ə)ɹ":"phon_schwar",
    "j(ʊ)":"phon_jschwu",
    "j(u)":"phon_jschwu",

    "d͡z":"phon_dz",
    "dz":"phon_dz",
    "d͡ʒ":"phon_dz_maj",
    "dʒ":"phon_dz_maj",
    "d͡ʐ":"phon_dz_retr",
    "dʐ":"phon_dz_retr",
    "d͡ʑ":"phon_dz_slash",
    "dʑ":"phon_dz_slash",
    "ɡ͡b":"phon_gb",
    "ɡb":"phon_gb",
	"ɡz":"phon_gz",
	"ɡʒ":"phon_gz_maj",
	"ɲ":"phon_j_maj",
    "k͡p":"phon_kp",
    "kp":"phon_kp",
    "k͡s":"phon_ks",
    "ks":"phon_ks",
	"kʃ":"phon_ks_maj",
    "k͡x":"phon_kx",
    "kx":"phon_kx",
    "kʷ":"phon_kw",
    "kw":"phon_kw",
	"lj":"phon_lj",
	"nj":"phon_nj",
    "p͡f":"phon_pf",
    "pf":"phon_pf",
	"sz":"phon_sz",
    "t͡sʰ":"phon_t_hs",
    "tsʰ":"phon_t_hs",
    "t͡ʂʰ":"phon_t_hs_retr",
    "tʂʰ":"phon_t_hs_retr",
    "t͡ɕʰ":"phon_t_hs_slash",
    "tɕʰ":"phon_t_hs_slash",
    "t͡ɬ":"phon_tk_maj",
    "tɬ":"phon_tk_maj",
    "d͡ɮ":"phon_tk_maj_slash",
    "dɮ":"phon_tk_maj_slash",
    "t͡s":"phon_ts",
    "ts":"phon_ts",
    "t͡sʼ":"phon_ts_chev2",
    "tsʼ":"phon_ts_chev2",
    "t͡ʃ":"phon_ts_maj",
    "tʃ":"phon_ts_maj",
    "t͡ʃʼ":"phon_ts_maj_chev2",
    "tʃʼ":"phon_ts_maj_chev2",
    "t͡ʂ":"phon_ts_retr",
    "tʂ":"phon_ts_retr",
    "t͡ɕ":"phon_ts_slash",
    "tɕ":"phon_ts_slash",
    "t͡θ":"phon_tt_maj",
    "tθ":"phon_tt_maj",
	"m͡p":"phon_mp",
	"m͡b":"phon_mb",
	"n͡t̪":"phon_nt_d",
	"n͡d̪":"phon_nd_d",
	"n͡t":"phon_nt",
	"n͡d":"phon_nd",
	"ɳ͡ʈ":"phon_n_retrt",
	"ɳ͡ɖ":"phon_n_retrd_retr",
	"ɲ͡c":"phon_j_majc",
	"ɲ͡ɟ":"phon_j_majj_maj_slash",
	"ŋ͡k":"phon_n_majk",
	"ŋ͡ɡ":"phon_n_majg",
	"ɴ͡q":"phon_n_maj_slashq",
	"ɴ͡ɢ":"phon_n_maj_slashg_maj_slash",
	"ŋ͡m":"phon_n_majm",
	"p͡ɸ":"phon_pp_slash",
	"b͡β":"phon_bb_maj",
	"b͡v":"phon_bv",
	"d͡ð":"phon_dd_maj",
	"ʈ͡ʂ":"phon_t_slashs_retr",
	"ɖ͡ʐ":"phon_d_slashz_retr",
	"c͡ç":"phon_cc_maj",
	"ɟ͡ʝ":"phon_j_maj_slashj_slash",
	"ɡ͡ɣ":"phon_gg_maj",
	"q͡χ":"phon_qx_maj",
	"ɢ͡ʁ":"phon_g_maj_slashr_maj",
	"ʔ͡h":"phon_glottstoph",
	"m͡ɸ":"phon_mp_slash",
	"m͡β":"phon_mb_maj",
	"n͡θ":"phon_nt_maj",
	"n͡ð":"phon_nd_maj",
	"n͡s":"phon_ns",
	"n͡z":"phon_nz",
	"n͡ʃ":"phon_ns_maj",
    "n͡ʒ":"phon_nz_maj",
    "mp":"phon_mp",
	"mb":"phon_mb",
	"nt̪":"phon_nt_d",
	"nd̪":"phon_nd_d",
	"nt":"phon_nt",
	"nd":"phon_nd",
	"ɳʈ":"phon_n_retrt",
	"ɳɖ":"phon_n_retrd_retr",
	"ɲc":"phon_j_majc",
	"ɲɟ":"phon_j_majj_maj_slash",
	"ŋk":"phon_n_majk",
	"ŋɡ":"phon_n_majg",
	"ɴq":"phon_n_maj_slashq",
	"ɴɢ":"phon_n_maj_slashg_maj_slash",
	"ŋm":"phon_n_majm",
	"pɸ":"phon_pp_slash",
	"bβ":"phon_bb_maj",
	"bv":"phon_bv",
	"dð":"phon_dd_maj",
	"ʈʂ":"phon_t_slashs_retr",
	"ɖʐ":"phon_d_slashz_retr",
	"cç":"phon_cc_maj",
	"ɟʝ":"phon_j_maj_slashj_slash",
	"ɡɣ":"phon_gg_maj",
	"qχ":"phon_qx_maj",
	"ɢʁ":"phon_g_maj_slashr_maj",
	"ʔh":"phon_glottstoph",
	"mɸ":"phon_mp_slash",
	"mβ":"phon_mb_maj",
	"nθ":"phon_nt_maj",
	"nð":"phon_nd_maj",
	"ns":"phon_ns",
	"nz":"phon_nz",
	"nʃ":"phon_ns_maj",
	"nʒ":"phon_nz_maj",
	"ŋʷ":"phon_n_majw",
	"ɡʷ":"phon_gw",
	"xʷ":"phon_xw",
    "ɣʷ":"phon_g_majw",
    "ŋw":"phon_n_majw",
	"ɡw":"phon_gw",
	"xw":"phon_xw",
    "ɣw":"phon_g_majw",
}