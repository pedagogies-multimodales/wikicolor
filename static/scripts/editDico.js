var dicoLang = document.getElementById('dicoLang').value;
var localDico = {}; // contiendra la liste des résultats de la recherche

// Récupération de la liste des modifications du dictionnaire
var logList = {};
function getLogFile() {
    $.ajax({
        url: '/getLog/',
        data: {
            lang: dicoLang
        },
        dataType: 'json',
        success: function(data) {
            logList = data['logs'];
            console.log(logList);
        },
        error: function(){
            console.log("Impossible de récupérer le log du dico !");
        }
    });
}


$(document).ready(function(){
    getLogFile();
    console.log('Ready.');

    $("#rechercher").click(function(){

        var mot = document.getElementById('inMot').value;
        var trans = document.getElementById('inTrans').value;
        var motCond = document.getElementById('condition-mot').value;
        var transCond = document.getElementById('condition-trans').value;
        var lastEntrShowed = 0;

        if (mot.length > 0 || trans.length > 0) {
            document.getElementById('resultTable').innerHTML = '<colgroup><col width="40%"><col width="40%"><col width="20%"></colgroup><tr><th>Mot</th><th>Transcription (API)</th><th>Actions</th></tr>';
            document.getElementById('nbres').innerHTML = '';
            document.getElementById('loader').style.display = '';
            document.body.style.cursor = "wait";

            console.log(dicoLang, motCond,mot,transCond,trans);

            //variable qui contiendra le dictionnaire de listes de graphies
            var reponseDico = new Object();

            //requête qui récupère la liste de graphies pour chaque phonème
            $.ajax({
                url: '/dicoSearch/',
                data: {
                    dicoLang: dicoLang,
                    mot: mot,
                    trans: trans,
                    motCond: motCond,
                    transCond: transCond
                },
                dataType: 'json',
                success: function(data) {listEntries(data)},
                error: function(){
                    document.getElementById('loader').style.display = 'none';
                    document.body.style.cursor = "default";
                    console.log("Problème de requête !");
                }
            });
            
        } else window.alert('Veuillez entrer une requête de recherche.');
        
        function listEntries(data) {
            localDico = data.listeEntrees;
            document.getElementById('loader').style.display = 'none';
            document.body.style.cursor = "default";
            console.log(data)

            document.getElementById('resultTable').innerHTML = '<colgroup><col width="40%"><col width="40%"><col width="20%"></colgroup><tr><th>Mot</th><th>Transcription (API)</th><th>Actions</th></tr>';
            document.getElementById('nbres').innerHTML = '('+Object.keys(data.listeEntrees).length+')';

            // on n'affiche que 50 résultats par page pour pas surcharger le navigateur
            showPart(lastEntrShowed,50,data.listeEntrees);
            if (Object.keys(data.listeEntrees).length > 50) {
                document.getElementById('btnShowMore').style.display = '';
                document.getElementById('btnShowMore').addEventListener('click',event => {
                    showPart(lastEntrShowed,50,data.listeEntrees);
                });
            }
        }

        function showPart(start,nb,dico) {
            var cptEntr = 0;
            nb = start + nb;
            for (i in dico) {
                cptEntr += 1;
                if(cptEntr > start && cptEntr <= nb) {
                    var entr = dico[i][0];

                    // Y a-t-il une trace de modification pour cette entrée ?
                    var logEntr = '';
                    for (log=0; log<logList.length; log++) {
                        if (logList[log]['word'] == entr) {
                            logEntr = '<img src="/static/im/history.png" class="logEntr" onclick="getLogOf(\''+entr+'\')" title="Historique des modifications">';
                        }
                    }

                    var lienWiki = '<a href="https://'+dicoLang+'.wiktionary.org/wiki/'+entr.replace(' ','_')+'" target="_blank"><button role="button" class="btn btn-sm" title="Consulter le Wiktionnaire">W</button></a>';
                    var transList = '';
                    
                    cptrows = 0
                    for (t in dico[i][1]['t']){
                        cptrows += 1
                        transList += "<span class='trans'>"+dico[i][1]['t'][t]['tt'] + "</span>"
                        if (dico[i][1]['t'][t]['tr']) { transList += " (" + dico[i][1]['t'][t]['tr'].join(', ') + ")"; }
                        if (dico[i][1]['t'][t]['tc']) { transList += '<img width="20px" src="/static/im/logo-comment.png" title="'+dico[i][1]['t'][t]['tc']+'"></img>';} //'<span style="font-size:.8em; font-style:italic;">' + thisWordTransList[t]['c'] +'</span>'
                        transList += '<br/>';
                    }
                    
                    var btnModifier = '<button id="btnModifier'+cptEntr+'" title="Modifier cette entrée" class="btn btn-primary btn-perso mx-3" onclick="modifEntry('+cptEntr+',\''+entr+'\')"><svg title="Modifier" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">  <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>  <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/></svg></button>';
                    var btnSupprimer = '<button id="btnSupprimer'+cptEntr+'" title="Supprimer cette entrée" class="btn btn-danger btn-perso mx-3" onclick="supprimer(\''+entr+'\')"><svg title="Supprimer" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">  <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>  <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/></svg></button>';
                    
                    var transInput = transList;//'<textarea class="api-kb inputTrans" rows="'+cptrows+'" disabled="true" type="text" id="td'+cptEntr+'">'+transList+'</textarea>';

                    document.getElementById('resultTable').innerHTML += '<tr id="tr'+cptEntr+'" class="tr"><td><span class="numEntr">'+cptEntr+'</span>'+lienWiki+' '+entr+'</td><td class="dicoReqRes">'+transInput+logEntr+'</td><td style="text-align:center">'+btnModifier+btnSupprimer+'</td></tr>';
                    getAPIkeyboard(dicoLang);
                }
            }
            lastEntrShowed = nb;
            if (nb >= Object.keys(dico).length) document.getElementById('btnShowMore').style.display = 'none';
        }    
        addStat("dicoSearch");
    });
});

function modifEntry(cptEntr,entr) {
    var modalEditWord = document.getElementById('modalEditWord')
    makeTransTable(entr,cptEntr);

    document.getElementById('popwMot').innerHTML = entr;
    if (localDico[cptEntr-1][1]['c']) { 
        document.getElementById('popwCatDiv').style.display = "block";
        document.getElementById('popwCat').innerHTML = localDico[cptEntr-1][1]['c']; 
    }
    if (localDico[cptEntr-1][1]['f']) { 
        document.getElementById('popwFanJianDiv').style.display = "";
        document.getElementById('popwFan').innerHTML = localDico[cptEntr-1][1]['f']; 
    }
    if (localDico[cptEntr-1][1]['j']) { 
        document.getElementById('popwFanJianDiv').style.display = "";
        document.getElementById('popwJian').innerHTML = localDico[cptEntr-1][1]['j']; 
    }
    if (localDico[cptEntr-1][1]['e']) { 
        document.getElementById('popwEDiv').style.display = "block";
        document.getElementById('popwE').innerHTML = localDico[cptEntr-1][1]['e']; 
    }
    
    document.getElementById('modalEditWordBtnAddTransDiv').innerHTML = '<button type="button" class="btn btn-primary roundButton" title="'+ langJson["sp_dicoEditEntryAdd"][dicoLang] +'" onclick="addNewTrans(\''+entr+'\','+cptEntr+')">+</button>'
    document.getElementById('modalEditWordBtnDiv').innerHTML = '<button id="modalEditWordBtnValider" class="btn btn-success" onclick="validEntry(\''+entr+'\','+cptEntr+')">Save</button>'

    modalEditWord.style.display = "block";
    getAPIkeyboard(dicoLang);
}

function makeTransTable(entr, cptEntr) {
    var thisWordTransList = localDico[cptEntr-1][1]['t'];
    document.getElementById('transTable').innerHTML = '<tr><th>Order</th><th>Transcription (IPA)</th><th>Region(s)/Variety</th><th><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16"><path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/><path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/></svg></th></tr>';
    for (t in thisWordTransList){
        var cmt = "";
        if (thisWordTransList[t]['tc']) { cmt = '<img width="20px" src="/static/im/logo-comment.png"></img><span style="font-size:.8em; font-style:italic;">' + thisWordTransList[t]['tc'] +'</span>' }
        var down = '<button type="button" onclick="thisTransDown(\''+entr+'\','+cptEntr+','+t+')" class="btn btn-light roundButton"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-down-circle" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v5.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V4.5z"/></svg></button>';
        var up = '<button type="button" onclick="thisTransUp(\''+entr+'\','+cptEntr+','+t+')" class="btn btn-light roundButton"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up-circle" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-7.5 3.5a.5.5 0 0 1-1 0V5.707L5.354 7.854a.5.5 0 1 1-.708-.708l3-3a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708L8.5 5.707V11.5z"/></svg></button>';
        var downup = down+up;
        var del = '<button type="button" onclick="delThisTrans(\''+entr+'\','+cptEntr+','+t+')" class="btn btn-danger roundButton"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16"><path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/><path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/></svg></button>';
        document.getElementById('transTable').innerHTML += '<tr><td>'+downup+'</td><td><input class="popwTrans inputTrans api-kb" onchange="updateTrans(this,'+cptEntr+','+t+')" type="text" value="'+ thisWordTransList[t]['tt'] +'"/></td><td style="cursor:pointer;" onclick="selectRegions(\''+entr+'\','+cptEntr+','+t+')"><span class="popwReg">'+ thisWordTransList[t]['tr'].join(', ') +'</span> <span class="popwCmt">'+ cmt +'</span></td><td>'+del+'</td></tr>'
    }
}

function updateTrans(thisInput, cptEntr, cptTrans) {
    localDico[cptEntr-1][1]['t'][cptTrans]['tt'] = thisInput.value.replace('g','ɡ');
    console.log('updateTrans:',localDico[cptEntr-1][1]['t'][cptTrans]['tt'])
}

function thisTransUp(entr, cptEntr, cptTrans) {
    if (cptTrans > 0) {
        var memTransUp = localDico[cptEntr-1][1]['t'][cptTrans-1];
        localDico[cptEntr-1][1]['t'][cptTrans-1] = localDico[cptEntr-1][1]['t'][cptTrans];
        localDico[cptEntr-1][1]['t'][cptTrans] = memTransUp;
        makeTransTable(entr,cptEntr);
    }
}
function thisTransDown(entr, cptEntr, cptTrans) {
    if (cptTrans < localDico[cptEntr-1][1]['t'].length-1) {
        var memTransDown = localDico[cptEntr-1][1]['t'][cptTrans+1];
        localDico[cptEntr-1][1]['t'][cptTrans+1] = localDico[cptEntr-1][1]['t'][cptTrans];
        localDico[cptEntr-1][1]['t'][cptTrans] = memTransDown;
        makeTransTable(entr,cptEntr);
    }
}
function delThisTrans(entr, cptEntr, cptTrans) {
    localDico[cptEntr-1][1]['t'].splice(cptTrans,1);
    makeTransTable(entr,cptEntr);
}

function addNewTrans(entr, cptEntr) {
    localDico[cptEntr-1][1]['t'].push({ 'tt':'', 'tr':[] });
    makeTransTable(entr,cptEntr);
    getAPIkeyboard(dicoLang);
}

function selectRegions(entr, cptEntr, cptTrans) {
    // Open the modal for selecting regions for this word pronunciation
    var modalEditWordRegion = document.getElementById('modalEditWordRegion')
    let transInfos = localDico[cptEntr-1][1]['t'][cptTrans];
    
    // Création des checkbox de région
    document.getElementById('regCheckboxDiv').innerHTML = "";
    for (reg in metaRegions[dicoLang]) {
        var r = reg;
        var l = metaRegions[dicoLang][reg];
        var newDiv = document.createElement('div');
        newDiv.classList = 'form-check form-check-inline';
        var newInp = document.createElement('input');
        newInp.classList = 'form-check-input wordRegionCheckbox';
        newInp.type = "checkbox";
        newInp.id = "wordRegion"+r;
        newInp.value = r;
        var newLab = document.createElement('label');
        newLab.classList = "form-check-label";
        newLab.for = "wordRegion"+r;
        newLab.title = l;
        newLab.innerHTML = "<img width='30px' class='me-1' style='border:1px solid black; border-radius:3px' src='/static/im/flag-"+r.toLowerCase()+".png'></img>" + r;

        if (transInfos['tr'].includes(r)) {
            newInp.checked = true;
        }

        newDiv.appendChild(newInp);
        newDiv.appendChild(newLab);
        document.getElementById('regCheckboxDiv').appendChild(newDiv);

        // <div class="form-check form-check-inline">
        //      <input class="form-check-input wordRegionCheckbox" type="checkbox" id="wordRegionUK" value="UK">
        //      <label class="form-check-label" for="wordRegionUK" title="United Kingdom - Received Pronunciation">UK</label>
        // </div>

    }
    

    document.getElementById('modalEditWordRegionWord').innerHTML = entr;
    document.getElementById('modalEditWordRegionTrans').innerHTML = transInfos['tt'];
    document.getElementById('modalEditWordRegionCptEntr').value = cptEntr;
    
    // AFFICHAGE DU COMMENTAIRE SI EXISTANT
    if (transInfos['tc']){
        document.getElementById('modalEditWordRegionComment').value = transInfos['tc'];
    } else {
        document.getElementById('modalEditWordRegionComment').value = '';
    }
    modalEditWordRegion.style.display = "block";
}

function validRegions() {
    var cptEntr = Number(document.getElementById('modalEditWordRegionCptEntr').value);
    var trans = document.getElementById('modalEditWordRegionTrans').innerHTML;
    var entr = document.getElementById('modalEditWordRegionWord').innerHTML;
    console.log(trans);
    var listCheckbox = document.querySelectorAll('.wordRegionCheckbox');
    var checkedRegions = [];
    var regList = localDico[cptEntr-1][1]['t'].filter(xtrans => (xtrans.tt == trans))[0]['tr'];

    for (cb in listCheckbox) {
        if (listCheckbox[cb].checked) {
            checkedRegions.push(listCheckbox[cb].value);
        }
    }

    // Mise à jour du localDico :
    for (r in metaRegions[dicoLang]) {
        // Pour chaque metaRegion (var globale) on regarde si checkée ou pas et on met à jour localdico en fonction
        if (checkedRegions.includes(r) && !regList.includes(r)) {
            // add the metaRegion if checked by user and not yet in localDico
            regList.push(r)
            
        } else if (!checkedRegions.includes(r) && regList.includes(r)) {
            // remove the metaRegion if found in localDico and not checked by user (→unchecked)
            regList.splice(regList.indexOf(r),1)
            
        }
    }

    var comment = document.getElementById('modalEditWordRegionComment').value;
    if (comment.length > 0) {
        if (localDico[cptEntr-1][1]['t'].filter(xtrans => (xtrans.tt == trans))[0]['tc']) { 
            // si il y a déjà un commentaire
            var cmt = localDico[cptEntr-1][1]['t'].filter(xtrans => (xtrans.tt == trans))[0]['tc'];
            if (comment != cmt) {
                console.log('Comment changed!', comment);
                localDico[cptEntr-1][1]['t'].filter(xtrans => (xtrans.tt == trans))[0]['tc'] = comment;
            }
        } else {
            console.log('New Comment!', comment);
            localDico[cptEntr-1][1]['t'].filter(xtrans => (xtrans.tt == trans))[0]['tc'] = comment;
        }
    }

    modifEntry(cptEntr, entr); // réinitialiser la fenêtre "Edit an entry"
    document.getElementById('modalEditWordRegion').style.display = "none"; // fermer la fenêtre "Regions"
}

async function validEntry(entr, cptEntr) {
    console.log('validEntry(',entr,cptEntr,')');
    document.getElementById('actionBlock').style.display = '';
    document.body.style.cursor = "wait";

    // Récupérer les nouveaux inputs (ex. transcriptions modifiées)
    var qPopwTrans = document.querySelectorAll('.popwTrans');
    for (el=0; el<qPopwTrans.length; el++) {
        console.log(el, qPopwTrans[el]);
        if (qPopwTrans[el].value != localDico[cptEntr-1][1]['t'][el]['tt']) {
            localDico[cptEntr-1][1]['t'][el]['tt'] = qPopwTrans[el].value;
            console.log("Modification de trans :", qPopwTrans[el].value);
        }
    }
    
    var colis = {
        "mot": localDico[cptEntr-1][0],
        "infos": localDico[cptEntr-1][1],
        "lang": dicoLang
    }
    console.log(colis);
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(colis)
    };
    const response = await fetch('/editEntry/', options)
	console.log('Envoi de la requête au serveur...');
    const data = await response.json();
        
    document.body.style.cursor = "default";
    console.log(data);
    window.alert(data.msg);
    logList.push(data.newlog);
    document.getElementById('actionBlock').style.display = 'none';
    document.getElementById('modalEditWord').style.display = "none";
    document.getElementById('rechercher').click();
}

function supprimer(entr, cptEntr) {
    var conf = window.confirm("Êtes-vous sûr⋅e de vouloir supprimer cette entrée du dictionnaire ?");
    if (conf) {
        document.getElementById('actionBlock').style.display = '';
        
        document.body.style.cursor = "wait";

        $.ajax({
            url: '/delEntry/',
            data: {
                mot: entr,
                lang: dicoLang
            },
            dataType: 'json',
            success: function(data) {
                document.body.style.cursor = "default";
                console.log(data.msg);
                window.alert(data.msg);
                logList.push(data.newlog);
                document.getElementById('actionBlock').style.display = 'none';
            },
            error: function(){
                // document.getElementById('loader').style.display = 'none';
                document.body.style.cursor = "default";
                window.alert("La suppression n'a pas pu être effectuée !");
                document.getElementById('actionBlock').style.display = 'none';
            }
        });
        document.getElementById('rechercher').click();
    }
}

function ajouter() {
    var newMot = document.getElementById('newWord').value;
    var newTrans = document.getElementById('newTrans').value;
    if (newMot.length > 0 && newTrans.length > 0) {
        validAddEntry(newMot,newTrans);
    } else {
        window.alert("Vous devez saisir un mot et au moins une transcription phonétique !");
    }
}

function validAddEntry(mot, trans) {
    document.getElementById('actionBlock').style.display = '';
    document.body.style.cursor = "wait";

    $.ajax({
        url: '/addEntry/',
        data: {
            mot: mot,
            trans: trans,
            lang: dicoLang
        },
        dataType: 'json',
        success: function(data) {
            document.body.style.cursor = "default";
            document.getElementById('actionBlock').style.display = 'none';
            console.log(data.msg);
			window.alert(data.msg);
            logList.push(data.newlog);
            document.getElementById('newWord').value = '';
            document.getElementById('newTrans').value = '';
            document.getElementById('inMot').value = mot;
            document.getElementById('inTrans').value = '';
            $('#rechercher').click();
        },
        error: function(){
            // document.getElementById('loader').style.display = 'none';
            document.body.style.cursor = "default";
            document.getElementById('actionBlock').style.display = 'none';
            window.alert("L'ajout n'a pas pu être effectué !");
        }
    });
}


//////////////////////////////////////
////////// PARAMÉTRAGE POPUP /////////
////////// POUR HISTORIQUE   /////////
//////////////////////////////////////
// Get the modal
var modal = document.getElementById("myModal");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// AFFICHAGE DES LOGS
// Fonction pour ouvrir le PopUp 
function getLogOf(entr) {
    if (entr=="*") {
        document.getElementById('popLogH2').innerHTML = '';
    } else {
        document.getElementById('popLogH2').innerHTML = 'Entrée : <i>"'+entr+'"</i>';
    }

    var myDataTableDiv = document.getElementById('myDataTable');
    myDataTableDiv.innerHTML = '<table class="table table-hover" id="logTable" data-order=\'[[ 0, "desc"]]\' data-page-length="25"><thead><tr><th scope="col">Date</th><th scope="col">Word</th><th scope="col">Type</th><th scope="col">Old version</th><th scope="col">New version</th></tr></thead><tbody id="logTableBody"></tbody></table>';
    
    // <table class="table table-hover" id="logTable" data-order='[[ 0, "desc"]]' data-page-length='25'>
    //     <thead>
    //         <tr>
    //             <th scope="col">Date</th>
    //             <th scope="col">Word</th>
    //             <!-- <th scope="col">User</th> -->
    //             <th scope="col">Type</th>
    //             <th scope="col">Old version</th>
    //             <th scope="col">New version</th>
    //         </tr>
    //     </thead>
    //     <tbody id="logTableBody">
    //     </tbody>
    // </table>

    
    modal.style.display = "block";

    // <td>Date</td>
    // <td>Word</td>
    // <td>User</td>
    // <td>Type</td>
    // <td>Old word</td>
    // <td>New word</td>

    var logTableBody = document.getElementById('logTableBody');
    logTableBody.innerHTML = "";

    var badgeClasses = {
        "MODIF": "bg-info text-dark",
        "ADD": "bg-success",
        "DEL": "bg-danger"
    }

    for (log in logList) {
        if (logList[log]['word'] == entr || entr=="*") {
            var [a,b] = logList[log]['date'].split('T');
            var [y,m,d] = a.split('-');
            var [H,M,S] = b.split(':');
            var logd = `${y}/${m}/${d} ${H}:${M}`;

            if (Array.isArray(logList[log]['old'])) {
                var old = '<span class="trans">'+ logList[log]['old'].join(', ') +'</span>';
            } else {
                var old = "";
                for (el in logList[log]['old']["t"]) {
                    old += '<span class="trans">'+ logList[log]['old']["t"][el]["tt"] +'</span> (' + logList[log]['old']["t"][el]["tr"].join(', ') + ')'
                    if (logList[log]['old']["t"][el]['tc']) { old += '<img width="20px" src="/static/im/logo-comment.png"></img><span style="font-size:.8em; font-style:italic;">'+ logList[log]['old']["t"][el]["tc"] + '</span>' }
                    old += "<br/>";
                }
            }

            if (Array.isArray(logList[log]['new'])) {
                var neww = '<span class="trans">'+ logList[log]['new'].join(', ') +'</span>';
            } else {
                var neww = "";
                for (el in logList[log]['new']["t"]) {
                    neww += '<span class="trans">'+ logList[log]['new']["t"][el]["tt"] +'</span> (' + logList[log]['new']["t"][el]["tr"].join(', ') + ')'
                    if (logList[log]['new']["t"][el]['tc']) { neww += '<img width="20px" src="/static/im/logo-comment.png"></img><span style="font-size:.8em; font-style:italic;">'+ logList[log]['new']["t"][el]["tc"] + '</span>' }
                    neww += "<br/>";
                }
            }

            logTableBody.innerHTML += "<tr><td>"+ logd +"</td><td>"+logList[log]['word']+"</td><td><span class='badge rounded-pill "+badgeClasses[logList[log]['type']]+"'>"+logList[log]['type']+"</span></td><td>"+old+"</td><td>"+neww+"</td></tr>";
        }
    }
    // Formattage du tableau
    // https://datatables.net/manual/options
    $('#logTable').DataTable();
    
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
    modalEditWord.style.display = "none";
}
// When the user clicks anywhere outside of the modal, close it
window.addEventListener('click', function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
})

//////////////////////////////////////
////////// PARAMÉTRAGE POPUP /////////
////////// POUR MODIF DE MOT /////////
//////////////////////////////////////
// Get the modal
var modalEditWord = document.getElementById("modalEditWord");

// Get the <span> element that closes the modal
var spanEditWord = document.getElementById("closeEditWord");

// When the user clicks on <span> (x), close the modal
spanEditWord.onclick = function() {
    modalEditWord.style.display = "none";
    document.getElementById('transTable').innerHTML = '<tr><th>Transcription (IPA)</th><th>Region(s)/Variety</th></tr>';
}
// When the user clicks anywhere outside of the modal, close it
window.addEventListener('click', function(event) {
    if (event.target == modalEditWord) {
        modalEditWord.style.display = "none";
        document.getElementById('transTable').innerHTML = '<tr><th>Transcription (IPA)</th><th>Region(s)/Variety</th></tr>';
    }
})

//////////////////////////////////////
////////// PARAMÉTRAGE POPUP /////////
//////// POUR MODIF DE REGION ////////
//////////////////////////////////////
// Get the modal
var modalEditWordRegion = document.getElementById("modalEditWordRegion");

// Get the <span> element that closes the modal
var spanEditWordRegion = document.getElementById("closeEditWordRegion");

// When the user clicks on <span> (x), close the modal
spanEditWordRegion.onclick = function() {
    modalEditWordRegion.style.display = "none";
    
}
// When the user clicks anywhere outside of the modal, close it
window.addEventListener('click', function(event) {
    if (event.target == modalEditWordRegion) {
        modalEditWordRegion.style.display = "none";
        
    }
})