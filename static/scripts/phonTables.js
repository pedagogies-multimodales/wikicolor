var phonTableFr = `
<div class="d-flex justify-content-evenly">
    <!-- CONSONNES -->
    <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px">
        <div class="form-check align-self-center">
            <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-cons')" id="checkAll" checked>
        </div>
        <table style="background-color:white;">
            <tbody>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="m"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="n"></div></td>
                    <td class="phonTableTd"> </td>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="ŋ"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="p"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="t"></div></td>
                    <td class="phonTableTd"> </td>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="k"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="b"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="d"></div></td>
                    <td class="phonTableTd"> </td>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="ɡ"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="f"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="s"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="ʃ"></div></td>
                    <td class="phonTableTd"> </td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="v"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="z"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="ʒ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="ʁ"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"> </td>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="l"></div></td>
                    <td class="phonTableTd"> </td>
                    <td class="phonTableTd"> </td>
                </tr>
                <tr>
                    <td class="phonTableTd"></td>
                    <td class="phonTableTd"></td>
                    <td class="phonTableTd"></td>
                    <td class="phonTableTd"></td>
                </tr>
                <tr>
                    <td class="phonTableTd"> </td>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="j"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="ɥ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="w"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"></td>
                    <td class="phonTableTd"></td>
                    <td class="phonTableTd"></td>
                    <td class="phonTableTd"></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="ks"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="ɡz"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="ɲ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonc" data-phon="tʃ"></div></td>
                </tr>
            </tbody>
        </table>
    </div>

    <!-- VOYELLES -->
    <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px">
        <div class="form-check align-self-center">
            <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-vow')" id="checkAllv" checked>
        </div>
        <table style="background-color:white;">
            <tbody>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="i"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="y"></div></td>
                    <td class="phonTableTd"> </td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="u"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="e"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ø"></div></td>
                    <td rowspan="2"><div class="custphon custphonv" data-phon="ə"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="o"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɛ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="œ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɔ"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"> </td>
                    <td class="phonTableTd"> </td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="a"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɑ"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"></td>
                    <td class="phonTableTd"></td>
                    <td class="phonTableTd"></td>
                    <td class="phonTableTd"></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɛ̃"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="œ̃"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɑ̃"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɔ̃"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"></td>
                    <td class="phonTableTd"></td>
                    <td class="phonTableTd"></td>
                    <td class="phonTableTd"></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="wa"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="wɛ̃"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɥi"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ij"></div></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
`

var phonTableEn = `
<div class="d-flex justify-content-evenly">
    <!-- CONSONNES -->
    <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px">
        <div class="form-check align-self-center">
            <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-cons')" id="checkAll" checked>
        </div>
        <table style="background-color:white;">
            <tbody>
            <tr>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="m"></div></td>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="n"></div></td>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="ŋ"></div></td>
            </tr>
            <tr>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="p"></div></td>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="t"></div></td>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="k"></div></td>
            </tr>
            <tr>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="b"></div></td>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="d"></div></td>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="ɡ"></div></td>
            </tr>
            <tr>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="f"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="θ"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="s"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="ʃ"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="x"></div></td>
            </tr>
            <tr>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="v"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="ð"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="z"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="ʒ"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="h"></div></td>
            </tr>
            <tr>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="l"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="ɹ"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="j"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="w"></div></td>
            </tr>
            <tr>
                <td class="phonTableTd" colspan="7">
                    <div class="custphon custphonc" data-phon="sz"></div>
                    <div class="custphon custphonc" data-phon="tθ"></div>
                    <div class="custphon custphonc" data-phon="ts"></div>
                    <div class="custphon custphonc" data-phon="tʃ"></div>
                    <div class="custphon custphonc" data-phon="dʒ"></div>
                    <div class="custphon custphonc" data-phon="ks"></div>
                    <div class="custphon custphonc" data-phon="kʃ"></div>
                    <div class="custphon custphonc" data-phon="kw"></div>
                    <div class="custphon custphonc" data-phon="ɡz"></div>
                    <div class="custphon custphonc" data-phon="ɡʒ"></div>
                    <div class="custphon custphonc" data-phon="nj"></div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <!-- VOYELLES -->
    <div class="d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px">
            <div class="form-check align-self-center">
                <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-vow')" id="checkAllv" checked>
            </div>
            <table style="background-color:white;">
                <tbody>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="iː"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɪ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ʊ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="uː"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="e"></div><div class="custphon custphonv" data-phon="ɛ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɜː"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɝ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɔː"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="æ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ʌ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɑː"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɒ"></div></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px">
            <div class="form-check align-self-center">
                <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-schwa')" id="checkAllschwa" checked>
            </div>
            <table style="background-color:white;">
                <tbody>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="i"></div></td>   
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="ə"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="ɚ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="jʊ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="u"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="əl"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="əm"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="ən"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="əz"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="ɪz"></div></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px; width:max-content">
            <div class="form-check align-self-center">
                <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-vownas')" id="checkAllvn" checked>
            </div>
            <table style="background-color:white;text-align:center">
                <tbody>
                    <tr>
                        <td class="phonTableTd" colspan="2"><div class="custphon custphonvn" data-phon="ɪə"></div></td>
                        <td class="phonTableTd" colspan="2"><div class="custphon custphonvn" data-phon="ʊə"></div></td>
                    </tr>
                    <tr>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="eɪ"></div></td>
                        <td class="phonTableTd" colspan="2" rowspan="2"><div class="custphon custphonvn" data-phon="əʊ"></div></td>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="ɔɪ"></div></td>
                    </tr>
                    <tr>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="eə"></div></td>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="oʊ"></div></td>
                    </tr>
                    <tr>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="aɪ"></div></td>
                        <td colspan="2" class="phonTableTd"><div class="custphon custphonvn" data-phon="aɪə"></div></td>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="aʊ"></div></td>
                    </tr>
                </tbody>
                </table>
        </div>
        <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px; width:max-content">
            <div class="form-check align-self-center">
                <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-vowOther')" id="checkAllvowOther" checked>
            </div>
            <table style="background-color:white;text-align:center">
                <tbody>
                    <tr>
                        <td class="phonTableTd">
                                                <div class="custphon custphonvowOther" data-phon="wʌ"></div>
                                                <div class="custphon custphonvowOther" data-phon="wɑː"></div>
                                                <div class="custphon custphonvowOther" data-phon="waɪ"></div>
                                                <div class="custphon custphonvowOther" data-phon="waɹ"></div>
                                                <div class="custphon custphonvowOther" data-phon="wa"></div>
                                            </td>
                    </tr>
                    <tr>
                        <td class="phonTableTd">
                                                <div class="custphon custphonvowOther" data-phon="juː"></div>
                                                <div class="custphon custphonvowOther" data-phon="jə"></div>
                                                <div class="custphon custphonvowOther" data-phon="jʊə"></div>
                                                <div class="custphon custphonvowOther" data-phon="jʊɹ"></div>
                                            </td>
                    </tr>
                    <tr>
                        <td class="phonTableTd">
                                                <div class="custphon custphonvowOther" data-phon="ɪɹ"></div>
                                                <div class="custphon custphonvowOther" data-phon="ʊɹ"></div>
                                                <div class="custphon custphonvowOther" data-phon="ɑɹ"></div>
                                                <div class="custphon custphonvowOther" data-phon="ɔɹ"></div>
                                            </td>
                    </tr>
                </tbody>
                </table>
        </div>
    </div>
</div>
`

var phonTableEnBr = `
<div class="d-flex justify-content-evenly">
    <!-- CONSONNES -->
    <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px">
        <div class="form-check align-self-center">
            <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-cons')" id="checkAll" checked>
        </div>
        <table style="background-color:white;">
            <tbody>
            <tr>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="m"></div></td>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="n"></div></td>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="ŋ"></div></td>
            </tr>
            <tr>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="p"></div></td>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="t"></div></td>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="k"></div></td>
            </tr>
            <tr>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="b"></div></td>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="d"></div></td>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="ɡ"></div></td>
            </tr>
            <tr>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="f"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="θ"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="s"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="ʃ"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="x"></div></td>
            </tr>
            <tr>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="v"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="ð"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="z"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="ʒ"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="h"></div></td>
            </tr>
            <tr>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="l"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="ɹ"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="j"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="w"></div></td>
            </tr>
            <tr>
                <td class="phonTableTd" colspan="7">
                    <div class="custphon custphonc" data-phon="sz"></div>
                    <div class="custphon custphonc" data-phon="tθ"></div>
                    <div class="custphon custphonc" data-phon="ts"></div>
                    <div class="custphon custphonc" data-phon="tʃ"></div>
                    <div class="custphon custphonc" data-phon="dʒ"></div>
                    <div class="custphon custphonc" data-phon="ks"></div>
                    <div class="custphon custphonc" data-phon="kʃ"></div>
                    <div class="custphon custphonc" data-phon="kw"></div>
                    <div class="custphon custphonc" data-phon="ɡz"></div>
                    <div class="custphon custphonc" data-phon="ɡʒ"></div>
                    <div class="custphon custphonc" data-phon="nj"></div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <!-- VOYELLES -->
    <div class="d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px">
            <div class="form-check align-self-center">
                <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-vow')" id="checkAllv" checked>
            </div>
            <table style="background-color:white;">
                <tbody>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="iː"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="e"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɜː"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="uː"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɔː"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɑː"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɪ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɛ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="æ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ʊ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɒ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ʌ"></div></td>   
                </tr>
                </tbody>
            </table>
        </div>
        <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px; width:max-content">
            <div class="form-check align-self-center">
                <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-vownas')" id="checkAllvn" checked>
            </div>
            <table style="background-color:white;text-align:center">
                <tbody>
                    <tr>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="e(ɪ)"></div></td>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="a(ʊ)"></div></td>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="ə(ʊ)"></div></td>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="ɔ(ɪ)"></div></td>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="a(ɪ)"></div></td>
                    </tr>
                    <tr>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="ɪ(ə)"></div></td>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="e(ə)"></div></td>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="ʊ(ə)"></div></td>
                        <td class="phonTableTd"></td>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="aɪ(ə)"></div></td>
                    </tr>
                </tbody>
                </table>
        </div>
        <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px">
            <div class="form-check align-self-center">
                <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-schwa')" id="checkAllschwa" checked>
            </div>
            <table style="background-color:white;">
                <tbody>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="(i/ɪ)"></div></td>   
                    <td class="phonTableTd" colspan="2"><div class="custphon custphonschwa" data-phon="(ə)"></div></td>

                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="(u/ʊ)"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="j(u/ʊ)"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="(ɪ)z"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="(ə)l"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="(ə)m"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="(ə)n"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="(ə)z"></div></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px; width:max-content">
            <div class="form-check align-self-center">
                <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-vowOther')" id="checkAllvowOther" checked>
            </div>
            <table style="background-color:white;text-align:center">
                <tbody>
                    <tr>
                        <td class="phonTableTd">
                                                <div class="custphon custphonvowOther" data-phon="wʌ"></div>
                                                <div class="custphon custphonvowOther" data-phon="wɑː"></div>
                                                <div class="custphon custphonvowOther" data-phon="wa(ɪ)"></div>
                                                <div class="custphon custphonvowOther" data-phon="wa"></div>
                                                <div class="custphon custphonvowOther" data-phon="juː"></div>
                                                <div class="custphon custphonvowOther" data-phon="ju"></div>
                                                <div class="custphon custphonvowOther" data-phon="j(ə)"></div>
                                                <div class="custphon custphonvowOther" data-phon="jʊ(ə)"></div>
                                            </td>
                    </tr>
                </tbody>
                </table>
        </div>
    </div>
</div>
`

var phonTableEnUS = `
<div class="d-flex justify-content-evenly">
    <!-- CONSONNES -->
    <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px">
        <div class="form-check align-self-center">
            <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-cons')" id="checkAll" checked>
        </div>
        <table style="background-color:white;">
            <tbody>
            <tr>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="m"></div></td>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="n"></div></td>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="ŋ"></div></td>
            </tr>
            <tr>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="p"></div></td>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="t"></div></td>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="k"></div></td>
            </tr>
            <tr>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="b"></div></td>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="d"></div></td>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="ɡ"></div></td>
            </tr>
            <tr>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="f"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="θ"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="s"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="ʃ"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="x"></div></td>
            </tr>
            <tr>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="v"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="ð"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="z"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="ʒ"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="h"></div></td>
            </tr>
            <tr>
                <td class="phonTableTd"> </td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="l"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="ɹ"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="j"></div></td>
                <td class="phonTableTd"><div class="custphon custphonc" data-phon="w"></div></td>
            </tr>
            <tr>
                <td class="phonTableTd" colspan="7">
                    <div class="custphon custphonc" data-phon="sz"></div>
                    <div class="custphon custphonc" data-phon="tθ"></div>
                    <div class="custphon custphonc" data-phon="ts"></div>
                    <div class="custphon custphonc" data-phon="tʃ"></div>
                    <div class="custphon custphonc" data-phon="dʒ"></div>
                    <div class="custphon custphonc" data-phon="ks"></div>
                    <div class="custphon custphonc" data-phon="kʃ"></div>
                    <div class="custphon custphonc" data-phon="kw"></div>
                    <div class="custphon custphonc" data-phon="ɡz"></div>
                    <div class="custphon custphonc" data-phon="ɡʒ"></div>
                    <div class="custphon custphonc" data-phon="nj"></div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <!-- VOYELLES -->
    <div class="d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px">
            <div class="form-check align-self-center">
                <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-vow')" id="checkAllv" checked>
            </div>
            <table style="background-color:white;">
                <tbody>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="i"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="e"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɝ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="u"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɑ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɔ"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɪ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ɛ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="æ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ʊ"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonv" data-phon="ʌ"></div></td>   
                </tr>
                </tbody>
            </table>
        </div>
        <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px; width:max-content">
            <div class="form-check align-self-center">
                <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-vownas')" id="checkAllvn" checked>
            </div>
            <table style="background-color:white;text-align:center">
                <tbody>
                    <tr>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="e(ɪ)"></div></td>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="a(ɪ)"></div></td>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="a(ʊ)"></div></td>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="ɔ(ɪ)"></div></td>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="o(ʊ)"></div></td>
                    </tr>
                    <tr>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="ɪɹ"></div></td>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="eɹ"></div></td>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="ʊɹ"></div></td>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="ɑɹ"></div></td>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="ɔɹ"></div></td>
                        <td class="phonTableTd"><div class="custphon custphonvn" data-phon="aɪ(ə)"></div></td>
                    </tr>
                </tbody>
                </table>
        </div>
        <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px">
            <div class="form-check align-self-center">
                <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-schwa')" id="checkAllschwa" checked>
            </div>
            <table style="background-color:white;">
                <tbody>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="(i/ɪ)"></div></td>   
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="(ə)"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="(ɚ)"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="(u/ʊ)"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="j(u/ʊ)"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="(ɪ)z"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="(ə)l"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="(ə)m"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="(ə)n"></div></td>
                    <td class="phonTableTd"><div class="custphon custphonschwa" data-phon="(ə)z"></div></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px; width:max-content">
            <div class="form-check align-self-center">
                <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-vowOther')" id="checkAllvowOther" checked>
            </div>
            <table style="background-color:white;text-align:center">
                <tbody>
                    <tr>
                        <td class="phonTableTd">
                                                <div class="custphon custphonvowOther" data-phon="wʌ"></div>
                                                <div class="custphon custphonvowOther" data-phon="wɑː"></div>
                                                <div class="custphon custphonvowOther" data-phon="wa(ɪ)"></div>
                                                <div class="custphon custphonvowOther" data-phon="waɹ"></div>
                                                <div class="custphon custphonvowOther" data-phon="wa"></div>
                                                <div class="custphon custphonvowOther" data-phon="juː"></div>
                                                <div class="custphon custphonvowOther" data-phon="ju"></div>
                                                <div class="custphon custphonvowOther" data-phon="j(ə)"></div>
                                                <div class="custphon custphonvowOther" data-phon="jʊɹ"></div>
                                            </td>
                    </tr>
                </tbody>
                </table>
        </div>
    </div>
</div>
`

var phonTableZh = ``