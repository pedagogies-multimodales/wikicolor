var langJson = {
    "ti_titre": {
        "fr":"WikiColor - Cliquez ici pour en savoir plus !",
        "en":"WikiColor - Clic here to know more!",
        "zh":"WikiColor - Clic here to know more!"
    },
    "sp_header": {
        "fr":"Cette application est un prototype en cours de conception. <a href='https://groups.google.com/d/forum/alem-app' target='_blank'><b>Accéder au forum</b></a> pour échanger avec la communauté et faire vos suggestions.",
        "en":"This application still is a prototype under development. <a href='https://groups.google.com/d/forum/alem-app' target='_blank'><b>Access to the forum</b></a> to share and make suggestions.",
        "zh":"该应用是目前正在开发的原型。 <a href='https://groups.google.com/d/forum/alem-app' target='_blank'><b>进入社群论坛</b></a>交流，提出您的建议。"
    },
    "sp_footcode": {
        "fr":"Code source",
        "en":"Source code",
        "zh":"Source code"
    },
    "sp_footjournal": {
        "fr":"Journal des modifications",
        "en":"Modifications log",
        "zh":"Modifications log"
    },
    "sp_footlicence": {
        "fr":"Code open source sous licence CC BY-NC-SA 4.0",
        "en":"Open source code under CC BY-NC-SA 4.0 licence",
        "zh":"Open source code under CC BY-NC-SA 4.0 licence"
    },
    "sp_footSpaCy": {
        "fr": "Analyse morphosyntaxique :",
        "en": "Syntactic analysis:",
        "zh": "句法分析: "
    },
    "sp_subtitle": {
        "fr": '<span class="phon_k">C</span><span class="phon_o_maj">o</span><span class="phon_l">l</span><span class="phon_o_maj">o</span><span class="phon_r_maj">r</span><span class="phon_i">i</span><span class="phon_z">s</span><span class="phon_9">eu</span><span class="phon_r_maj">r</span><span> </span><span class="phon_f">Ph</span><span class="phon_o_maj">o</span><span class="phon_n">n</span><span class="phon_o_maj">o</span><span class="phon_g">g</span><span class="phon_r_maj">r</span><span class="phon_a">a</span><span class="phon_f">ph</span><span class="phon_e">é</span><span class="phon_m">m</span><span class="phon_i">i</span><span class="phon_k">que</span>',
        "en": '<span class="phon_f">ph</span><span class="phon_arobase schwa">o</span><span class="phon_n">n</span><span class="phon_q_maj stress2">o</span><span class="phon_g">g</span><span class="phon_r_slash">r</span><span class="phon_cbrack">a</span><span class="phon_f">ph</span><span class="phon_i_long stress1">e</span><span class="phon_m">m</span><span class="phon_i_maj schwa">i</span><span class="phon_k">c</span><span> </span><span class="phon_k">c</span><span class="phon_v_maj stress2">o</span><span class="phon_l">l</span><span class="phon_arobase schwa">o</span><span class="phon_r_slash">r</span><span class="phon_aschwi stress1">i</span><span class="phon_z">s</span><span class="phon_arobase schwa">er</span>',
        "zh": ''
    },
    "sp_bicolor": {
        "fr": '<span style="color: #ff0000; -webkit-text-stroke-width: 0.5px; -webkit-text-stroke-color: #000000;">B</span><span style="color: #40b9ff; -webkit-text-stroke-width: 0.5px; -webkit-text-stroke-color: #000000;">i</span><span style="color: #ff0000; -webkit-text-stroke-width: 0.5px; -webkit-text-stroke-color: #000000;">c</span><span style="color: #40b9ff; -webkit-text-stroke-width: 0.5px; -webkit-text-stroke-color: #000000;">o</span><span style="color: #ff0000; -webkit-text-stroke-width: 0.5px; -webkit-text-stroke-color: #000000;">l</span><span style="color: #40b9ff; -webkit-text-stroke-width: 0.5px; -webkit-text-stroke-color: #000000;">o</span><span style="color: #ff0000; -webkit-text-stroke-width: 0.5px; -webkit-text-stroke-color: #000000;">re</span>',
        "en": '<span style="color: #ff0000; -webkit-text-stroke-width: 0.5px; -webkit-text-stroke-color: #000000;">B</span><span class="stress1" style="color: #40b9ff; -webkit-text-stroke-width: 0.5px; -webkit-text-stroke-color: #000000;">i</span><span style="color: #ff0000; -webkit-text-stroke-width: 0.5px; -webkit-text-stroke-color: #000000;">c</span><span class="schwa" style="color: #40b9ff; -webkit-text-stroke-width: 0.5px; -webkit-text-stroke-color: #000000;">o</span><span style="color: #ff0000; -webkit-text-stroke-width: 0.5px; -webkit-text-stroke-color: #000000;">l</span><span class="schwa" style="color: #40b9ff; -webkit-text-stroke-width: 0.5px; -webkit-text-stroke-color: #000000;">o</span><span style="color: #ff0000; -webkit-text-stroke-width: 0.5px; -webkit-text-stroke-color: #000000;">r</span>'
    },
    "sp_silentWay": {
        "fr": '<span style="-webkit-text-stroke-width: 0.5px; -webkit-text-stroke-color: #000000;"><span class="text phon_s">S</span><span style="background:-webkit-linear-gradient(white 60%, #e7236d 75%);-webkit-background-clip: text;background-clip: text;-webkit-text-fill-color: transparent;">i</span><span class="text phon_l">l</span><span class="text phon_e_maj ">e</span><span class="text phon_n">n</span><span class="text phon_t">t</span><span class="text punct "> </span><span class="text phon_w">W</span><span class="text phon_ei_maj">ay</span></span>',
        "en": '<span style="-webkit-text-stroke-width: 0.5px; -webkit-text-stroke-color: #000000;"><span class="text phon_s">S</span><span class="stress1" style="background:-webkit-linear-gradient(white 60%, #e7236d 75%);-webkit-background-clip: text;background-clip: text;-webkit-text-fill-color: transparent;">i</span><span class="text phon_l">l</span><span class="text phon_e_maj schwa">e</span><span class="text phon_n">n</span><span class="text phon_t">t</span><span class="text punct "> </span><span class="text phon_w">W</span><span class="text phon_ei_maj">ay</span></span>',
        "zh": '<span style="-webkit-text-stroke-width: 0.5px; -webkit-text-stroke-color: #000000;"><span class="text phon_s">S</span><span style="background:-webkit-linear-gradient(white 60%, #e7236d 75%);-webkit-background-clip: text;background-clip: text;-webkit-text-fill-color: transparent;">i</span><span class="text phon_l">l</span><span class="text phon_e_maj ">e</span><span class="text phon_n">n</span><span class="text phon_t">t</span><span class="text punct "> </span><span class="text phon_w">W</span><span class="text phon_ei_maj">ay</span></span>'
    },
    "sp_consigne": {
        "fr": "Entrer du texte à coloriser :",
        "en": "Enter some text to colorize:",
        "zh": "请键入一段文字："
    },
    "sp_btnColoriser": {
        "fr": "Coloriser",
        "en": "Colorize",
        "zh": "着色",
    },
    "sp_btnErase": {
        "fr": "Effacer",
        "en": "Erase",
        "zh": "重置"
    },
    "sp_output": {
        "fr": "Résultat :",
        "en": "Output:",
        "zh": "结果:"
    },
    "ti_btnCouleurFond": {
        "fr": "Changer la couleur du fond",
        "en": "Switch background color",
        "zh": "切换背景色"
    },
    "ti_btnCopierColler": {
        "fr": "Copier/Coller le contenu",
        "en": "Copy/paste content"
    },
    "ti_btnLiaison": {
        "fr": "Afficher les liaisons",
        "en": ""
    },
    "ti_btnBold": {
        "fr": "Activer/désactiver la police en gras",
        "en": "Switch font boldness"
    },
    "ti_btnzi": {
        "zh": "展示汉字"
    },
    "ti_btnwordspace": {
        "zh": "展示单词空间"
    },
    "ti_multitrans": {
        "fr": "changer de prononciation",
        "en": "switch pronunciation"
    },
    "ti_add2dico": {
        "fr": "Ajouter ce mot au dictionnaire",
        "en": "Add this word to the dictionary",
        "zh": ""
    },
    "sp_infoWiki": {
        "fr": "WikiColor est un coloriseur automatique de texte basé sur les transcriptions phonétiques du Wiktionnaire, du <a href='https://repository.ortolang.fr/api/content/morphalou/2/LISEZ_MOI.html' target='_blank'>lexique Morphalou3.1</a> et de la collaboration de nombreux utilisateurs. Vous pouvez consulter et participer à l'amélioration du dictionnaire en cliquant sur le bouton ci-dessous. L'alignement phono-graphémique reste encore expérimental, toute suggestion de votre part est bienvenue ! N'hésitez pas à visiter <a href='https://alem.hypotheses.org/outils-alem-app/wikicolor'>notre site</a>, et participez aux discussions sur <a href='https://groups.google.com/forum/#!forum/alem-app'>le forum</a>.",
        "en": "WikiColor is an automatic text coloriser based on Wiktionary's phonetic transcriptions, <a href='http://www.speech.cs.cmu.edu/cgi-bin/cmudict/?in=pronunciation&stress=-s' target='_blank'>CMU Pronouncing Dictionary</a>, <a href='https://github.com/JoseLlarena/Britfone' target='_blank'>Britfone Pronunciation Dictionary</a> and collaboration of our users community. Please consider to add words and phonetic transcriptions to our word base! Phono-graphemic alignment still is experimental, especially for English, any suggestion from you will be welcome! We are still working on the phonological representation of English, which is a big challenge. Please let us know your suggestions in <a href='https://groups.google.com/forum/#!forum/alem-app'>our Forum</a>. We also have <a href='https://alem.hypotheses.org/outils-alem-app/wikicolor'>a website</a> (English content will come soon!).",
        "zh": "WikiColor is an automatic text coloriser based on <a href='https://cc-cedict.org/wiki/' target='_blank'>the CC-CEdict Dictionnary</a>, the Python phonetiser <a href='https://pypi.org/project/pinyin/' target='_blank'>Pinyin0.4.0</a> and collaboration of our users community. Please consider to add words and phonetic transcriptions to our word base! Please let us know your suggestions in <a href='https://groups.google.com/forum/#!forum/alem-app'>our Forum</a>. We also have <a href='https://alem.hypotheses.org/outils-alem-app/wikicolor'>a website</a> (English content will come soon!)."
    },
    "sp_btnEditDico": {
        "fr": "Consulter/éditer les dictionnaires",
        "en": "Search/edit WikiColor's dictionaries",
        "zh": "查询/编辑辞典"
    },
    "sp_footDic": {
        "fr": "Dictionnaire (issu du Wiktionnaire) : ",
        "en": "Dictionary (from Wiktionary's database): ",
        "zh": "词典:"
    },
    "sp_footAli": {
        "fr": "Aligneur phonographémique : ",
        "en": "Phonographemic aligner: ",
        "zh": "Phonographemic aligner: "
    },
    "sp_tuto": {
        "fr": "Accéder aux tutoriels WikiColor",
        "en": "Access WikiColor's tutorials",
        "zh": "访问教程"
    },
    "sp_copypasteH2": {
        "fr": "Copier/coller le résultat",
        "en": "Copy/Paste the output",
        "zh": "Copy/Paste the output"
    },
    "sp_copypasteP": {
        "fr": "Copier/coller le texte ci-dessous dans LibreOffice, Word ou dans un e-mail, par exemple.",
        "en": "Copy/Paste the texte below to LibreOffice, Word or to an e-mail, for example.",
        "zh": "Copy/Paste the texte below to LibreOffice, Word or to an e-mail, for example."
    },
    "myTooltip": {
        "fr": "Copier",
        "en": "Copy",
        "zh": "Copy"
    },
    "sp_copypasteCopier": {
        "fr": "Copier",
        "en": "Copy",
        "zh": "Copy"
    },
    "sp_copypasteCodeSource": {
        "fr": "Code Source",
        "en": "Source Code",
        "zh": "Source Code"
    },
    "myTooltipWhite": {
        "fr": "Copier",
        "en": "Copy",
        "zh": "Copy"
    },
    "sp_copypasteCopierWhite": {
        "fr": "Copier",
        "en": "Copy",
        "zh": "Copy"
    },
    "sp_copypasteCodeSourceWhite": {
        "fr": "Code Source",
        "en": "Source Code",
        "zh": "Source Code"
    },
    "sp_popAddwHead": {
        "fr": "Ajouter ce mot au dictionnaire de WikiColor",
        "en": "Add this word to WikiColor's dictionary",
        "zh": "Add this word to WikiColor's dictionary"
    },
    "sp_popAddwWord": {
        "fr": "Mot :",
        "en": "Word:",
        "zh": "Word:"
    },
    "sp_popAddwTrans": {
        "fr": "Transcription phonétique (API) :",
        "en": "Phonetic transcription (IPA):",
        "zh": "Pinyin (ex. ni2 hao3):"
    },
    "sp_popAddwInfo": {
        "fr": "(Vous pouvez saisir plusieurs transcriptions différentes en les séparant par une virgule)",
        "en": "(You can write different possible phonetic transcriptions between commas)",
        "zh": "(You can write different possible phonetic transcriptions between commas)"
    },
    "sp_popAddwAdd": {
        "fr": "Ajouter",
        "en": "Add",
        "zh": "Add"
    },
    "alert_addAtLeastOneTrans": {
        "fr": "Ajoutez au moins une transcription phonétique pour pouvoir enregistrer la nouvelle entrée dans le dictionnaire.",
        "en": "Add at least one transcription!",
        "zh": "Add at least one transcription!"
    },
    "ti_bugMark": {
        "fr": "Le Fidel utilisé ne permet pas l'alignement de",
        "en": "The Fidel used doesn't allow the alignment of",
        "zh": "The Fidel used doesn't allow the alignment of"
    },
    "sp_LookUpDicos": {
        "fr": "Consulter/éditer les dictionnaires WikiColor",
        "en": "Search/edit WikiColor's dictionaries",
        "zh": "Search/edit WikiColor's dictionaries"
    },
    "sp_LookUpDicosInfo": {
        "fr": "WikiColor se base sur des dictionnaires phonétiques pour coloriser les mots. Ces dictionnaires sont assemblés à partir de différentes sources, et nous vous invitons à les enrichir ! Merci d'avance pour votre collaboration.",
        "en": "WikiColor uses pronunciation dictionaries to colorise words. Those dictionaries are made from different sources, and we invite you to improve them! Thank you for your collaboration.",
        "zh": "WikiColor uses pronunciation dictionaries to colorise words. Those dictionaries are made from different sources, and we invite you to improve them! Thank you for your collaboration."
    },
    "sp_LookUpDicosRetour": {
        "fr": "Retour au coloriseur",
        "en": "Back to the coloriser",
        "zh": "Back to the coloriser"
    },
    "sp_dicoH2": {
        "fr": "Rechercher une entrée du dictionnaire français",
        "en": "Search words in the English dictionary",
        "zh": "中文詞典中搜索詞語"
    },
    "sp_dicoContient": {
        "fr": "contient",
        "en": "includes",
        "zh": "includes"
    },
    "sp_dicoComm": {
        "fr": "commence par",
        "en": "starts by",
        "zh": "starts by"
    },
    "sp_dicoFini": {
        "fr": "finit par",
        "en": "ends by",
        "zh": "ends by"
    },
    "sp_dicoEgal": {
        "fr": "est égal à",
        "en": "is equal to",
        "zh": "is equal to"
    },
    "sp_dicoContient2": {
        "fr": "contient",
        "en": "includes",
        "zh": "includes"
    },
    "sp_dicoComm2": {
        "fr": "commence par",
        "en": "starts by",
        "zh": "starts by"
    },
    "sp_dicoFini2": {
        "fr": "finit par",
        "en": "ends by",
        "zh": "ends by"
    },
    "sp_dicoEgal2": {
        "fr": "est égal à",
        "en": "is equal to",
        "zh": "is equal to"
    },
    "sp_dicoRechercher": {
        "fr": "Rechercher",
        "en": "Search",
        "zh": "Search"
    },
    "rechercher": {
        "fr": "Rechercher dans le dictionnaire",
        "en": "Look up this word and/or this transcription in the dictionary",
        "zh": "Look up this word and/or this transcription in the dictionary"
    },
    "sp_dicoInfoEntry": {
        "fr": "Nombre actuel d'entrées du dictionnaire",
        "en": "Current number of entries in the dictionary",
        "zh": "Current number of entries in the dictionary"
    },
    "sp_dicoInfoMod": {
        "fr": "modifications enregistrées par les utilisateurs",
        "en": "changes made by users",
        "zh": "changes made by users"
    },
    "sp_dicoInfoModAdd": {
        "fr": "ajouts",
        "en": "additions",
        "zh": "additions"
    },
    "sp_dicoInfoModEd": {
        "fr": "éditions",
        "en": "editings",
        "zh": "editings"
    },
    "sp_dicoInfoModDel": {
        "fr": "suppressions",
        "en": "removals",
        "zh": "removals"
    },
    "sp_dicoInfoHist": {
        "fr": "Voir l'historique complet des modifications du dictionnaire",
        "en": "Access to complete records of changes",
        "zh": "Access to complete records of changes"
    },
    "sp_dicoResultats": {
        "fr": "Résultats de la recherche",
        "en": "Search results",
        "zh": "Search results"
    },
    "sp_dicoMot": {
        "fr": "Mot :",
        "en": "Word:",
        "zh": "Word:"
    },
    "sp_dicoTrans": {
        "fr": "Transcription phonétique (API) :",
        "en": "Phonetic transcription (IPA):",
        "zh": "Pinyin (ex. ni2 hao3):"
    },
    "sp_dicoMot2": {
        "fr": "Mot :",
        "en": "Word:",
        "zh": "Word:"
    },
    "sp_dicoTrans2": {
        "fr": "Transcription phonétique (API) :",
        "en": "Phonetic transcription (IPA):",
        "zh": "Pinyin (ex. ni2 hao3):"
    },
    "sp_dicoActions": {
        "fr": "Actions",
        "en": "Actions",
        "zh": "Actions"
    },
    "btnShowMore": {
        "fr": "afficher plus de résultats",
        "en": "show more results",
        "zh": "show more results"
    },
    "sp_dicoAddNewWord": {
        "fr": "Ajouter une nouvelle entrée dans le dictionnaire",
        "en": "Add a new entry to WikiColor dictionary",
        "zh": "Add a new entry to WikiColor dictionary"
    },
    "sp_dicoAddNewWordAdd": {
        "fr": "Ajouter",
        "en": "Add",
        "zh": "Add"
    },
    "ti_addNewWord": {
        "fr": "Ajouter cette entrée dans le dictionnaire",
        "en": "Add this new entry to WikiColor dictionary",
        "zh": "Add this new entry to WikiColor dictionary"
    },
    "sp_dicoBackDico": {
        "fr": "Retour aux dictionnaires",
        "en": "Back to the dictionaries",
        "zh": "Back to the dictionaries"
    },
    "sp_dicoBackCol": {
        "fr": "Retour au coloriseur",
        "en": "Back to the coloriser",
        "zh": "Back to the coloriser"
    },
    "sp_dicoHisto": {
        "fr": "Historique des modifications",
        "en": "Record of changes",
        "zh": "Record of changes"
    },
    "sp_dicoEditEntry": {
        "fr": "Modifier une entrée du dictionnaire",
        "en": "Edit an entry of the dictionary",
        "zh": "Edit an entry of the dictionary"
    },
    "sp_dicoEditEntryAdd": {
        "fr": "Ajouter une prononciation",
        "en": "Add a pronunciation",
        "zh": "Add a pronunciation"
    },
    "sp_dicoEditEntryWord": {
        "fr": "Mot :",
        "en": "Word:",
        "zh": "Word:"
    },
    "sp_dicoEditEntryPOS": {
        "fr": "Traits morphosyntaxiques :",
        "en": "Part of speech:",
        "zh": "Part of speech:"
    },
    "sp_dicoEditEntryMeaning": {
        "fr": "Meaning",
        "en": "Meaning",
        "zh": "Meaning"
    },
    "sp_dicoEditEntryTrans": {
        "fr": "Transcription (API)",
        "en": "Transcription (IPA)",
        "zh": "Transcription (Pinyin)"
    },
    "sp_dicoEditEntryReg": {
        "fr": "Région(s)/Variété",
        "en": "Region(s)/Variety",
        "zh": "Region(s)/Variety"
    },
    "sp_dicoEditRegTitle": {
        "fr": "Sélectionnez une ou plusieurs régions pour cette prononciation",
        "en": "Select one or more regions for this pronunciation",
        "zh": "Select one or more regions for this pronunciation"
    },
    "sp_dicoEditRegW": {
        "fr": "Mot :",
        "en": "Word:",
        "zh": "Word:"
    },
    "sp_dicoEditRegP": {
        "fr": "Prononciation",
        "en": "Pronunciation",
        "zh": "Pronunciation"
    },
    "sp_dicoEditRegC": {
        "fr": "Commentaire :",
        "en": "Comment:",
        "zh": "Comment:"
    },
    "modalEditWordRegionComment": {
        "fr": "Vous pouvez commenter cette prononciation.",
        "en": "Type any comment you want on this pronunciation.",
        "zh": "Type any comment you want on this pronunciation."
    },
    "sp_dicoEditRegSave": {
        "fr": "Enregistrer",
        "en": "Save",
        "zh": "Save"
    },
    "ti_btnCustom": {
        "fr": "Personnaliser la colorisation",
        "en": "Customise the output",
        "zh": "Customise the output"
    },
    "sp_customExplain": {
        "fr": "Choisissez les phonèmes à coloriser et leur couleur",
        "en": "Choisissez les phonèmes à coloriser et leur couleur",
        "zh": "Choisissez les phonèmes à coloriser et leur couleur"
    },
    "sp_customColBase": {
        "fr": "Couleur de base :",
        "en": "Base color:",
        "zh": "Base color:"
    },
    "sp_popParHead": {
        "fr": "Exporter mes paramètres",
        "en": "Export my settings",
        "zh": "Export my settings"
    },
    "sp_popParInfo": {
        "fr": "Le lien ci-dessous vous permettra d'ouvrir WikiColor avec les paramètres actuels. Vous pouvez conserver ce lien dans un document ou l'envoyer à d'autres personnes. Il sera bientôt possible d'importer les couleurs dans les autres applications ALeM.",
        "en": "The link below will allow you to open WikiColor with the current settings.",
        "zh": "The link below will allow you to open WikiColor with the current settings."
    },
    "myTooltipPar": {
        "fr": "Copier",
        "en": "Copy",
        "zh": "Copy"
    },
    "sp_copypasteCopierPar": {
        "fr": "Copier",
        "en": "Copy",
        "zh": "Copy"
    },
    "ti_partager": {
        "fr": "Exporter/sauvegarder mes paramètres",
        "en": "Export/save my settings",
        "zh": "Export/save my settings"
    },
    "sp_btnExportCustom": {
        "fr": "Exporter/Enregistrer",
        "en": "Export/Save",
        "zh": "Export/Save"
    },
    "sp_customSpace": {
        "fr": "Taille des espaces entre les mots :",
        "en": "Length of spaces between words:",
        "zh": "Length of spaces between words:"
    }
}