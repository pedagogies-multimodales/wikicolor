"""coloriseur URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from colorapp import views as colorapp_views
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    path('admin/', admin.site.urls),
    path('app/', colorapp_views.redirApp),
    path('', colorapp_views.main0),
    re_path(r'^(?P<pageLang>fr|en|zh)/', colorapp_views.main),
    re_path(r'^(?P<pageLang>fr|en|zh)/<str:mySettings>', colorapp_views.main),
    path('colorize/', csrf_exempt(colorapp_views.colorize)),
    path('getPhonoOf/', csrf_exempt(colorapp_views.getPhonoOf)),
    path('_api/getPhonoOf/', colorapp_views.getPhonoOfGETrequest), # for direct request cURL etc.
    path('app/dico/', colorapp_views.dicoView),
    path('app/dico/fr', colorapp_views.dicoViewFr),
    path('app/dico/en', colorapp_views.dicoViewEn),
    path('app/dico/zh', colorapp_views.dicoViewZh),
    path('dicoSearch/', colorapp_views.dicoReq),
    path('checkWord/', colorapp_views.checkWord),
    path('checkWordList/', csrf_exempt(colorapp_views.checkWordList)),
    path('editEntry/', csrf_exempt(colorapp_views.editEntry)),
    path('addEntry/', colorapp_views.newEntry),
    path('delEntry/', colorapp_views.supprEntry),
    path('getLog/', colorapp_views.getLog),
    path('addStat/', csrf_exempt(colorapp_views.addStat))
]
