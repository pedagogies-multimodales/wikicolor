# Application WikiColor

Plus d'infos : https://alem.hypotheses.org/outils-alem-app/wikicolor

# Caractéristiques techniques 

Django 5.1 Python 3.10
cf. dépendances dans `requirements.txt`

# Conception

    Sylvain Coulange

Le traitement des liaisons en français a été pensé et développé par :

    Sanda Hachana
    Cynthia Rakotoarisoa
    Ning Zhang

Avec les suggestions et les crashtests bienveillants de :

    Alexandre Do
    Robert Jeannard
    Yoann Goudin
    Émilie Magnat

# Licence & code source

Le code de WikiColor et de l’aligneur phon2graph est sous licence Creative Common BY-NC-SA 4.0. Le dictionnaire phonétique est sous licences Creative Commons Attribution-ShareAlike 3.0 Unported License (CC-BY-SA) et GNU Free Documentation License (GFDL), conformément aux recommandations du Wiktionnaire.
